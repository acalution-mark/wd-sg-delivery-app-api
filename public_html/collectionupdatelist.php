<html>
    <head>
        <title>Collection Update List!</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style>
            #collection-list {
                width: calc(50% - 4px);
                float:left;
                border: 1px black solid;
            }
            #collection-order {
                width: calc(50% - 4px);
                float:right;
                border: 1px black solid;
            }
        </style>
    </head>
    <body>
        <div>Simple Collection Update List Page</div>
        <button type="button" id="collection-list-button">Load Collection List!</button>
        <hr>
        <div id="collection-wrapper" >
            <div id="collection-list" >
            </div>
            <div id="collection-order" >
            </div>
        </div>
        <script>
            $( document ).ready(function() {

                $( "#collection-list-button" ).click(function() {

                    var JSONObj = {
                        
                        "data":{
                            0:{
                                "purchase_order_num":'PO1611141738484M1',
                                "status": "COLLECTED",
                                "user_display_name": "Larry Simple API",
                                "user_id": "2",
                            },
                            1:{
                                "purchase_order_num":'PO1611111658182M1',
                                "status": "COLLECTED",
                                "user_display_name": "Larry Simple API",
                                "user_id": "2",
                            },
                            2:{
                                "purchase_order_num":'PO1611091558891M1',
                                "status": "OPEN",
                                "user_display_name": "Larry Simple API",
                                "user_id": "2",
                            }
                        }
                    };

                    var st = JSON.stringify(JSONObj);
                    
                    $.ajax({
                        type: "POST",
                        url: 'http://' + 'wd-dev2-deliapp.edlution.com.sg' + '/php/api/collection_u_l.php',
                        data: { data : st },
                        dataType   : 'json',
                        success: function(response) {
                            console.log(response);

                            if(response.ack == 1) {

                            }
                        },
                        error: function() {
                            console.log("Process Inputs Error.");
                        }
                    });
                });

            });
        </script>
    </body>
</html>

