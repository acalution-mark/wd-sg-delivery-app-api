<?php

namespace Joelvardy;

/**
 * RESTful routes
 *
 * @link	https://github.com/joelvardy/routes
 * @author	Joel Vardy <info@joelvardy.com>
 */
class Routes {


	protected $_parameters = array();
	protected $_toRun;
	protected $_notFound;
	protected $_allowedMethods = array(
		'get',
		'head',
		'post',
		'put',
		'delete',
		'trace',
		'options',
		'connect',
		'patch'
	);


	/**
	 * Clean up the formatting of the HTTP method
	 *
	 * @param	string [$method] (optional) The string to clean, if none is passed the current request method will be used
	 * @return	string The formatted HTTP method
	 */
	protected function getMethod($method = null) {

		// If no method was passed, we want the request method
		if ( ! $method) {
			$method = $_SERVER['REQUEST_METHOD'];
		}

		return trim(strtolower($method));

	}


	/**
	 * Clean up the formatting of the request URI
	 *
	 * @return	string The formatted request URI
	 */
	protected function requestUri() {

		// If we have a redirect URL we'll use that - it doesn't contain the query string
		return trim(isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['REQUEST_URI']);

	}


	/**
	 * Check whether the passed route matches the current request URI
	 *
	 * @param	string [$route] The route to check against
	 * @return	boolean
	 */
	protected function checkRoute($route) {

		// Check whether it's a straight match
		if ($route == $this->requestUri()) {
			return true;
		}

		// Perform a regular expression match
		if (preg_match('/^'.str_replace('/', '\/', $route).'$/', $this->requestUri(), $parameters)) {

			// Remove all but the subpattern matches
			unset($parameters[0]);

			$this->_parameters = $parameters;

			return true;

		}

		return false;

	}


	/**
	 * If a passed route matches the request, set the callback
	 *
	 * @param	string [$method] The method to check
	 * @param	string [$route] The route to check against
	 * @param	object [$callback] The callback to be run if the route matches
	 * @return	boolean If the route matches true, otherwise false
	 */
	protected function add($method, $route, $callback) {

		// Check the request method matches
		if ($method == $this->getMethod()) {

			// Check whether the route matches
			if ($this->checkRoute($route)) {

				// Well done guys, set the callback
				$this->_toRun = $callback;

				return true;

			}

		}

		return false;

	}


	/**
	 * Add a route
	 *
	 * @param	string [$method] The method of the route
	 * @param	string [$arguments] The route and callback
	 * @return	boolean If the route matches true, otherwise false
	 */
	public function __call($method, $arguments) {

		// Ensure the method is allowed
		if (in_array($this->getMethod($method), $this->_allowedMethods)) {
			return $this->add($this->getMethod($method), $arguments[0], $arguments[1]);
		}

		return false;

	}


	/**
	 * Set the not found callback
	 *
	 * @param	object [$callback] The callback to be run if no other route matches a request
	 * @return	void
	 */
	public function notFound($callback) {
		$this->_notFound = $callback;
	}


	/**
	 * Run routes
	 *
	 * @return	void
	 */
	public function run() {

		if ($this->_toRun) {

			$status = call_user_func_array($this->_toRun, $this->_parameters);

		}

		if ( ! $this->_toRun || (isset($status) && ! $status)) {

			// Set headers
			header('HTTP/1.1 404 Not Found');

			if ($this->_notFound) call_user_func_array($this->_notFound, $this->_parameters);

		}

	}

}
