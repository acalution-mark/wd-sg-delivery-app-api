<?php 

// For Tracking User Logins
// session_start();
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Log4PHP
require_once('/var/www/log4php/Logger.php');
Logger::configure('/var/www/html3/public_html/php/log4php_cfg.xml');

$logger = Logger::getLogger("mainLog");
// $logger->debug("Main Logger initiatised");

$cronlogger = Logger::getLogger("cronLog");
// $logger->debug("Cron Logger initiatised");

$deliveryAppLogger = Logger::getLogger("deliveryAppLog");
// $logger->debug("Delivery App Logger initiatised");

// For HTTP_PATH Variable & MySQL
require "config.php";

// Firebug
require "ext/FirePHPCore/fb.php";

// PHPMailer
require "ext/PHPMailer-master/PHPMailerAutoload.php";

// FPDF
require "ext/fpdf17/fpdf.php";

// For Datatables Server Side
require("classes/ssp.class.php"); 

// Helper Functions
require "functions.php";

// Helper Classes
require "classes/DBHelper.php";
require "classes/EmailHelper.php";
require "classes/PDFHelper.php";
require "classes/HTMLHelper.php";
require "classes/FileUploadTemp.php";

// Module Classes
require "classes/User.php";
require "classes/Collection.php";
require "classes/Deliveries.php";

?>
