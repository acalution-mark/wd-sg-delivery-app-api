<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_POST,'$_POST');
// fb($_FILES, 'FILES');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "COLLECTION CREATE UPDATE DELETE");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$deliveryAppLogger->debug($_POST,'$_POST');

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    // Validations
    $errors = array();
    unset($errors);
    $err_msg = "";

    $required_fields = array(
        "action"
    );

    foreach($required_fields as $field) {
        // $deliveryAppLogger->debug("field: [{$field}]");

        if(isset($data[$field])){

            // $deliveryAppLogger->debug("value: [{$data[$field]}]");

        } else {
            $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
            $errors[$field] = fieldname_as_text($field) . " can't be blank";
            $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
        }

    } // foreach($required_fields as $field) {

    if(!empty($errors)){
        fb("Missing Required Fields.");
        $deliveryAppLogger->error("Missing Required Fields.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Missing Required Fields.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    // Create a new object
    $collection = new Collection($deliveryAppLogger, $dbConn);

    $json_res_data = array();
    $json_res_data["ack"] = 1;
    $json_res_data["err"] = 1;
    $json_res_data["err_msg"] = "Process Action Error.";
    $json_res_data["action"] = $data['action'];


    if($data['action'] == "create"){

    }
    else if($data['action'] == "update"){

        $deliveryAppLogger->debug("action: [update]");

        //More Validations
        $errors = array();
        unset($errors);
        $err_msg = "";

        $required_fields = array(
            "status",
            "purchase_order_num",
            "user_display_name",
            "user_id"
        );

        foreach($required_fields as $field) {
            // $deliveryAppLogger->debug("field: [{$field}]");

            if(isset($data[$field])){

                // $deliveryAppLogger->debug("value: [{$data[$field]}]");

            } else {
                $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
                $errors[$field] = fieldname_as_text($field) . " can't be blank.";
                $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
            }

        } // foreach($required_fields as $field) {

        if(!empty($errors)){
            fb("Missing Further Required Fields.");
            $deliveryAppLogger->error("Missing Further Required Fields.");

            $json_res_data["ack"] = 1;
            $json_res_data["err"] = 1;
            $json_res_data["err_msg"] = "Missing Further Required Fields.";

            $deliveryAppLogger->error($json_res_data);
            $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

            echo json_encode($json_res_data);
            return;
        }

        $json_res_data = $collection->update($data); 

        if($json_res_data["err"] == 1){
            fb($json_res_data["err_msg"]);
            $deliveryAppLogger->error($json_res_data["err_msg"]);
            $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

            echo json_encode($json_res_data);
            return;
        }   //If $json_res_data["err"] == 0, Successful Updated

    }
    else if($data['action'] == "delete"){

    } 
    else {
        $deliveryAppLogger->error("Action [{$data['action']}] invalid.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Action [{$data['action']}] invalid.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;

    }

    sleep(1);

    // Set Return JSON Array Data
    $json_res_data["ack"] = 1;
    
}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}

$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>
