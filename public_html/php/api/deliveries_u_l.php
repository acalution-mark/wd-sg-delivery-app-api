<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_POST,'$_POST');
// fb($_FILES, 'FILES');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "DELIVERIES UPDATE LIST");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$deliveryAppLogger->debug($_POST,'$_POST');

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    // Validations
    $errors = array();
    unset($errors);
    $err_msg = "";

    $required_fields = array(
        "data"
    );

    foreach($required_fields as $field) {
        // $deliveryAppLogger->debug("field: [{$field}]");

        if(isset($data[$field])){

            // $deliveryAppLogger->debug("value: [{$data[$field]}]");

        } else {
            $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
            $errors[$field] = fieldname_as_text($field) . " can't be blank";
            $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
        }

    } // foreach($required_fields as $field) {

    if(!empty($errors)){
        fb("Missing Required Fields.");
        $deliveryAppLogger->error("Missing Required Fields.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Missing Required Fields.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    // Create a new object
    $deliveries = new Deliveries($deliveryAppLogger, $dbConn);

    $json_res_data = array();
    $json_res_data["ack"] = 1;
    $json_res_data["err"] = 1;
    $json_res_data["err_msg"] = "Process Action Error.";

    $json_res_data = $deliveries->updateList($data); 

    if($json_res_data["err"] == 1){
        $deliveryAppLogger->error($json_res_data["err_msg"]);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }   //If $json_res_data["err"] == 0, Successful Updated



    sleep(1);

    // Set Return JSON Array Data
    $json_res_data["ack"] = 1;

}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}

$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>
