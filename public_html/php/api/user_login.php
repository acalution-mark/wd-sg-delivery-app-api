<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "LOGIN");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$deliveryAppLogger->debug($_POST,'$_POST');

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    // Validations
    $errors = array();
    unset($errors);
    $err_msg = "";

    $required_fields = array("username","password");

    foreach($required_fields as $field) {
        // $deliveryAppLogger->debug("field: [{$field}]");

        if(isset($data[$field])){

            // $deliveryAppLogger->debug("value: [{$data[$field]}]");

        } else {
            $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
            $errors[$field] = fieldname_as_text($field) . " can't be blank.";
            $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
        }

    } // foreach($required_fields as $field) {

    if(!empty($errors)){

        $deliveryAppLogger->error("Attempt Log in Failed");

        $json_res_data["ack"] = 1;
        $json_res_data["logged_in"] = 0;

        foreach($errors as $key => $error){
            $json_res_data["result"] .= " " . htmlentities($error);
        }

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;

    }

    // Attempt Login
    // Save Login Credentials
    $username = $data['username'];
    $password = $data['password'];
    $user_type = 'account';
    $deliveryAppLogger->info("username: [{$username}] password: [{$password}]");

    $user = new User($deliveryAppLogger,$dbConn); // Create new object
    if(!is_object($user)){
        $deliveryAppLogger->error("login not object");

        $json_res_data["ack"] = 0;
        $json_res_data["logged_in"] = 0;

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    $userRecord = $user->login($user_type,$username,$password);
    if(!$userRecord){
        $deliveryAppLogger->error("Attempt Log in Failed");

        $json_res_data["ack"] = 1;
        $json_res_data["logged_in"] = 0;

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    //$deliveryAppLogger->debug($userRecord);

    // Reach here means user login successful

    // Set Return variables
    $deliveryAppLogger->info("Attempt Log in Successful");
    $json_res_data["logged_in"] = 1;
    $json_res_data["user_id"] = $userRecord['user_id'];
    $json_res_data["user_display_name"] = $userRecord['display_name'];
    $json_res_data["user_email"] = $userRecord['email_addr'];
    
    $json_res_data["ack"] = 1;
    
    sleep(1);
    
} else {
    $json_res_data["ack"] = 0;
    $json_res_data["logged_in"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}

$deliveryAppLogger->info('returned json_encode');
$deliveryAppLogger->info(json_encode($json_res_data));
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>