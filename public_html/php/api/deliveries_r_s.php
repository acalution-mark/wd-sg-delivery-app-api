<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "GET DELIVERIES SINGLE READ");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$deliveryAppLogger->debug($_POST,'$_POST');

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    // Validations
    $errors = array();
    unset($errors);
    $err_msg = "";

    $required_fields = array("sale_order_num");

    foreach($required_fields as $field) {
        // $deliveryAppLogger->debug("field: [{$field}]");

        if(isset($data[$field])){

            // $deliveryAppLogger->debug("value: [{$data[$field]}]");

        } else {
            $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
            $errors[$field] = fieldname_as_text($field) . " can't be blank.";
            $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
        }

    } // foreach($required_fields as $field) {

    if(!empty($errors)){
        //fb("Missing Required Fields.");
        $deliveryAppLogger->error("Missing Required Fields.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Missing Required Fields.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    $returnData = array(); // Set Data to return

    // Retrieving Data 
    $dbhelper = new DBHelper($deliveryAppLogger,$dbConn);

    // Get Data from table [admin_discount]
    $table = 'v_admin_orders';

    $soRecord = $dbhelper->getSingleRowWhereSingleColumn(
        $table, // table name
        "sale_order_num", // where column name
        "str", // where column datatype, int|str
        $data['sale_order_num']); // where column value

    if(!$soRecord){
        $deliveryAppLogger->error("getRowsCount [{$table}] Failed.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);

        return;
    }

    $delivery_date_formated = strtotime($soRecord['delivery_datetime']);
    $delivery_date_formated = date("d F Y", $delivery_date_formated);
    $delivery_date_formated .= " " . $soRecord['prefered_delivery_timeslot'];
    
    // Get Data from table [v_orders_products_products]
    $table = "v_orders_products_products";
    $where_array = array(
        array( 
            "fieldname" => "sale_order_num",
            "bColons" => true,
            "value" => $soRecord['sale_order_num'])
    );
    $consumer_product = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);
    
    $items = array();
    $cnt = 0;
    foreach($consumer_product as $key => $value){
        if($value['year'] == 1) $value['year'] = 'Non-Vintage';
        if($value['region_display_name'] != NULL) $value['region_display_name'] = ", {$value['region_display_name']}";
        if($value['country_display_name'] != NULL) $value['country_display_name'] = ", {$value['country_display_name']}";
        
        $items[$cnt]['name'] = $value['name'];
        $items[$cnt]['wine_information'] = $value['variety_display_name'] . " " . $value['year'] . $value['country_display_name'] . $value['region_display_name'];
        $items[$cnt]['quantity'] = $value['quantity'];
        
        $cnt++;
    }


    $returnData['delivery_datetime'] = $delivery_date_formated;
    $returnData['address_line_1'] = $soRecord['delivery_address_line_1'];
    $returnData['address_line_2'] = $soRecord['delivery_unit_number'];
    $returnData['postal_code'] = $soRecord['delivery_postal_code'];
    $returnData['first_name'] = $soRecord['delivery_first_name'];
    $returnData['last_name'] = $soRecord['delivery_last_name'];
    $returnData['consumer_id'] = $soRecord['user_id'];
    $returnData['contact_num'] = $soRecord['delivery_contact_num_1_phone'];
    $returnData['has_signed'] = $soRecord['has_signed'];
    if($returnData['has_signed']){
        $returnData['signature_path'] = 'consumer_signatures/' . $soRecord['sale_order_num'] . '.jpg';
    }
    
    $returnData['products'] = $items;
    
    $returnData['delivery_status'] = $soRecord['status'];
    
    // Set Return JSON Array Data
    $json_res_data["data"] = $returnData;
    $json_res_data["ack"] = 1;
    
    sleep(1);
    
}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}

$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>