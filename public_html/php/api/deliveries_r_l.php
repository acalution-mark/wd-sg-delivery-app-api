<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "GET DELIVERY LIST");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {
    
    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    $dbhelper = new DBHelper($deliveryAppLogger,$dbConn);
    $table = 'v_admin_orders';
    /*$rows = $dbhelper->getAllRows($table, 'delivery_datetime', 'desc');

    if(!$rows < 0){
        $deliveryAppLogger->error("Get table [{$table}] Failed.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 0;
        $json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }*/

    
    $sql = "SELECT * FROM {$table} WHERE ";
    $where1 = "delivery_datetime >= STR_TO_DATE('{$data['start_date_time']}', '%d-%m-%Y %H:%i:%s') ";
    $sql .= $where1;
    $where2 = "AND delivery_datetime <= STR_TO_DATE('{$data['end_date_time']}', '%d-%m-%Y %H:%i:%s') ";
    $sql .= $where2;
    $sql .= "Order By delivery_datetime desc";
    
    $deliveryAppLogger->debug('$sql: [' . $sql . ']');
    
    $result_set = mysqli_query($dbConn, $sql);
    $rows = array();
    
    while($row = mysqli_fetch_assoc($result_set)){

        $rows[] = $row;

    }
    $rows_count = count($rows);
    // $deliveryAppLogger->debug("Returned rows count: [{$rows_count}]");

    $list = array();
    $cnt = 0;

    // $deliveryAppLogger->debug("##Start FOREACH##");
    foreach($rows as $key => $row){
        // $deliveryAppLogger->debug("#New Row: [{$cnt}]#");

        $delivery_date = date('d M Y', strtotime($row['delivery_datetime']));

        $list[$cnt]['sale_order_num'] = $row['sale_order_num'];
        $list[$cnt]['delivery_datetime'] = $delivery_date . ' ' . $row['prefered_delivery_timeslot'];
        $list[$cnt]['status'] = $row['status'];
        
        $list[$cnt]['address_line_1'] = $row['delivery_address_line_1'];
        $list[$cnt]['address_line_2'] = $row['delivery_unit_number'];
        $list[$cnt]['postal_code'] = $row['delivery_postal_code'];
        $list[$cnt]['first_name'] = $row['delivery_first_name'];
        $list[$cnt]['last_name'] = $row['delivery_last_name'];
        $list[$cnt]['consumer_id'] = $row['user_id'];
        $list[$cnt]['contact_num'] = $row['delivery_contact_num_1_phone'];
        $list[$cnt]['has_signed'] = $row['has_signed'];
        if($list[$cnt]['has_signed']){
            $list[$cnt]['signature_path'] = 'consumer_signatures/' . $row['sale_order_num'] . '.jpg';
        }
        
        // Get Data from table [v_orders_products_products]
        $table = "v_orders_products_products";
        $where_array = array(
            array( 
                "fieldname" => "sale_order_num",
                "bColons" => true,
                "value" => $row['sale_order_num'])
        );
        $consumer_product = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);

        $items = array();
        $cnt1 = 0;
        foreach($consumer_product as $key => $value){
            if($value['year'] == 1) $value['year'] = 'Non-Vintage';
            if($value['region_display_name'] != NULL) $value['region_display_name'] = ", {$value['region_display_name']}";
            if($value['country_display_name'] != NULL) $value['country_display_name'] = ", {$value['country_display_name']}";

            $items[$cnt1]['name'] = $value['name'];
            $items[$cnt1]['wine_information'] = $value['variety_display_name'] . " " . $value['year'] . $value['country_display_name'] . $value['region_display_name'];
            $items[$cnt1]['quantity'] = $value['quantity'];

            $cnt1++;
        }
        
        $list[$cnt]['products'] = $items;

        $cnt++;
    } // End of Loop
    // $deliveryAppLogger->debug("##End FOREACH##");

    // Set Return JSON Array Data
    $json_res_data["data"] = $list;
    $json_res_data["ack"] = 1;

    sleep(1);
    
}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}


$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>