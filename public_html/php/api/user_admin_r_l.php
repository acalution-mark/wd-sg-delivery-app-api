<?php 

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "GET ADMIN USER LIST");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {
    
    $dbhelper = new DBHelper($deliveryAppLogger,$dbConn);
    $table = 'users_admin';
    $where_array = array(
            array( 
                "fieldname" => "has_deli_rights",
                "bColons" => false,
                "value" => 1),
    );
    $rows = $dbhelper->getRows($table, $where_array, NULL, NULL, null, null);

    if(!$rows < 0){
        $deliveryAppLogger->error("Get table [{$table}] Failed.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 0;
        $json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }
        
    $rows_count = count($rows);
    // $deliveryAppLogger->debug("Returned rows count: [{$rows_count}]");

    $list = array();
    $cnt = 0;

    // $deliveryAppLogger->debug("##Start FOREACH##");
    foreach($rows as $key => $row){
        // $deliveryAppLogger->debug("#New Row: [{$cnt}]#");

        $list[$cnt]["user_id"] = $row['user_id'];
        $list[$cnt]["user_display_name"] = $row['display_name'];
        $list[$cnt]["user_email"] = $row['email_addr'];
        $list[$cnt]["username"] = $row['username'];
        

        $cnt++;
    } // End of Loop
    // $deliveryAppLogger->debug("##End FOREACH##");

    // Set Return JSON Array Data
    $json_res_data["data"] = $list;
    $json_res_data["ack"] = 1;

    sleep(1);
    
}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}


$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>