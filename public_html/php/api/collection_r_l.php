<?php

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "GET COLLECTION LIST");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    $dbhelper = new DBHelper($deliveryAppLogger,$dbConn);
    $table = 'v_collections_admin_ui';
    /*$rows = $dbhelper->getAllRows($table, 'collection_datetime', 'desc');

    if(!$rows < 0){
        $deliveryAppLogger->error("Get table [{$table}] Failed.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 0;
        $json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }*/

    $sql = "SELECT * FROM {$table} WHERE ";
    $where1 = "collection_datetime >= STR_TO_DATE('{$data['start_date_time']}', '%d-%m-%Y %H:%i:%s') ";
    $sql .= $where1;
    $where2 = "AND collection_datetime <= STR_TO_DATE('{$data['end_date_time']}', '%d-%m-%Y %H:%i:%s') ";
    $sql .= $where2;
    $sql .= "Order By collection_datetime desc";

    $deliveryAppLogger->debug('$sql: [' . $sql . ']');

    $result_set = mysqli_query($dbConn, $sql);
    $rows = array();

    while($row = mysqli_fetch_assoc($result_set)){

        $rows[] = $row;

    }

    $rows_count = count($rows);
    // $deliveryAppLogger->debug("Returned rows count: [{$rows_count}]");

    $list = array();
    $cnt = 0;

    // $deliveryAppLogger->debug("##Start FOREACH##");
    foreach($rows as $key => $row){
        // $deliveryAppLogger->debug("#New Row: [{$cnt}]#");
        $deliveryAppLogger->debug($row);

        $collection_date = date('d M Y', strtotime($row['collection_datetime']));

        $list[$cnt]['purchase_order_num'] = $row['purchase_order_num'];
        $list[$cnt]['collection_datetime'] = $collection_date . ' ' . $row['prefered_collection_timeslot'];
        $list[$cnt]['status'] = $row['status'];

        // Get Data from table [addresses_merchants]
        $table = "addresses_merchants";
        $where_array = array(
            array(
                "fieldname" => "merchant_company_id",
                "bColons" => false,
                "value" => $row['merchant_company_id']),
            array(
                "fieldname" => "type",
                "bColons" => true,
                "value" => 'collection')
        );
        $collection_address = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);

        $list[$cnt]['address_line_1'] = $collection_address[0]['address_line_1'];
        $list[$cnt]['address_line_2'] = $collection_address[0]['address_line_2'];
        $list[$cnt]['postal_code'] = $collection_address[0]['postal_code'];

        $list[$cnt]['first_name'] = $row['first_name'];
        $list[$cnt]['last_name'] = $row['last_name'];
        $list[$cnt]['contact_num'] = $row['contact_num_1_phone'];
        $list[$cnt]['shop_status'] = $row['status_opened_closed_display_name'];

        // Get Data from table [v_orders_products_products]
        $table = "v_orders_products_products";
        $where_array = array(
            array(
                "fieldname" => "merchant_company_id",
                "bColons" => false,
                "value" => $row['merchant_company_id']),
            array(
                "fieldname" => "sale_order_num",
                "bColons" => true,
                "value" => $row['sale_order_num'])
        );
        $merchant_product = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);

        $items = array();
        $cnt1 = 0;
        foreach($merchant_product as $key => $value){
            if($value['year'] == 1) $value['year'] = 'Non-Vintage';
            if($value['region_display_name'] != NULL) $value['region_display_name'] = ", {$value['region_display_name']}";
            if($value['country_display_name'] != NULL) $value['country_display_name'] = ", {$value['country_display_name']}";

            $items[$cnt1]['name'] = $value['name'];
            $items[$cnt1]['wine_information'] = $value['variety_display_name'] . " " . $value['year'] . $value['country_display_name'] . $value['region_display_name'];
            $items[$cnt1]['quantity'] = $value['quantity'];

            $cnt1++;
        }

        $list[$cnt]['products'] = $items;

        unset($merchant_product);
        $cnt++;
    } // End of Loop
    // $deliveryAppLogger->debug("##End FOREACH##");

    // Set Return JSON Array Data
    $json_res_data["data"] = $list;
    $json_res_data["ack"] = 1;

    sleep(1);

}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}


$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>
