<?php

require "../includes.php";

// fb($_SERVER,'$_SERVER');
// fb($_REQUEST,'$_REQUEST');
// fb($_POST,'$_POST');
// fb($_SESSION,'$_SESSION');

$deliveryAppLogger->info("START: [" . __FILE__ . "]");

define("PROCESS", "GET COLLECTION SINGLE READ");

$deliveryAppLogger->info("[PROCESS " . PROCESS . ":]");

$deliveryAppLogger->debug($_POST,'$_POST');

$json_res_data = array(); // Set Return Data

if($_SERVER['REQUEST_METHOD'] == 'POST'
   && isset($_POST)) {

    $data = json_decode($_POST['data'], true);
    $deliveryAppLogger->debug($data,'json_decode, $data');

    // Validations
    $errors = array();
    unset($errors);
    $err_msg = "";

    $required_fields = array("purchase_order_num");

    foreach($required_fields as $field) {
        // $deliveryAppLogger->debug("field: [{$field}]");

        if(isset($data[$field])){

            // $deliveryAppLogger->debug("value: [{$data[$field]}]");

        } else {
            $deliveryAppLogger->error(fieldname_as_text($field) . " can't be blank");
            $errors[$field] = fieldname_as_text($field) . " can't be blank.";
            $err_msg .=  fieldname_as_text($field) . " can't be blank.\n";
        }

    } // foreach($required_fields as $field) {

    if(!empty($errors)){
        //fb("Missing Required Fields.");
        $deliveryAppLogger->error("Missing Required Fields.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Missing Required Fields.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);
        return;
    }

    $returnData = array(); // Set Data to return

    // Retrieving Data
    $dbhelper = new DBHelper($deliveryAppLogger,$dbConn);

    // Get Data from table [v_collections_admin_ui]
    $table = 'v_collections_admin_ui';

    $poRecord = $dbhelper->getSingleRowWhereSingleColumn(
        $table, // table name
        "purchase_order_num", // where column name
        "str", // where column datatype, int|str
        $data['purchase_order_num']); // where column value

    if(!$poRecord){
        $deliveryAppLogger->error("getRowsCount [{$table}] Failed.");

        $json_res_data["ack"] = 1;
        $json_res_data["err"] = 1;
        $json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";

        $deliveryAppLogger->error($json_res_data);
        $deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

        echo json_encode($json_res_data);

        return;
    }

    $purchase_date_formated = strtotime($poRecord['created_at']);
    $purchase_date_formated = date("d F Y g:iA", $purchase_date_formated);
    $collection_date_formated = strtotime($poRecord['collection_datetime']);
    $collection_date_formated = date("d F Y", $collection_date_formated);

    $currency_format = "S$";

    $poRecord['gst_inclusive_amount'] = $currency_format . number_format(($poRecord['total_amt'] / (100 + ($poRecord['gst_rate_percentage'] * 100))) * ($poRecord['gst_rate_percentage'] * 100), 2);
    $poRecord['total_amt'] = $currency_format . $poRecord['total_amt'];
    $poRecord['purchase_date'] = $purchase_date_formated;
    $poRecord['collection_date_time'] = $collection_date_formated. " " . $poRecord['prefered_collection_timeslot'];

    // Get Data from table [addresses_merchants]
    $table = "addresses_merchants";
    $where_array = array(
        array(
            "fieldname" => "merchant_company_id",
            "bColons" => false,
            "value" => $poRecord['merchant_company_id']),
        array(
            "fieldname" => "type",
            "bColons" => true,
            "value" => 'collection')
    );
    $collection_address = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);
    $poRecord['collection'] = $collection_address[0];

    // Get Data from table [addresses_merchants]
    $table = "addresses_merchants";
    $where_array = array(
        array(
            "fieldname" => "merchant_company_id",
            "bColons" => false,
            "value" => $poRecord['merchant_company_id']),
        array(
            "fieldname" => "type",
            "bColons" => true,
            "value" => 'billing')
    );
    $billing_address = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);
    $poRecord['billing'] = $billing_address[0];

    // Get Data from table [v_orders_products_products]
    $table = "v_orders_products_products";
    $where_array = array(
        array(
            "fieldname" => "merchant_company_id",
            "bColons" => false,
            "value" => $poRecord['merchant_company_id']),
        array(
            "fieldname" => "sale_order_num",
            "bColons" => true,
            "value" => $poRecord['sale_order_num'])
    );
    $merchant_product = $dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);

    $items = array();
    $cnt = 0;
    foreach($merchant_product as $key => $value){
        if($value['year'] == 1) $value['year'] = 'Non-Vintage';
        if($value['region_display_name'] != NULL) $value['region_display_name'] = ", {$value['region_display_name']}";
        if($value['country_display_name'] != NULL) $value['country_display_name'] = ", {$value['country_display_name']}";

        $items[$cnt]['name'] = $value['name'];
        $items[$cnt]['wine_information'] = $value['variety_display_name'] . " " . $value['year'] . $value['country_display_name'] . $value['region_display_name'];
        $items[$cnt]['quantity'] = $value['quantity'];

        $cnt++;
    }

    $returnData['address_line_1'] = $collection_address[0]['address_line_1'];
    $returnData['address_line_2'] = $collection_address[0]['address_line_2'];
    $returnData['postal_code'] = $collection_address[0]['postal_code'];
    $returnData['first_name'] = $poRecord['first_name'];
    $returnData['last_name'] = $poRecord['last_name'];
    $returnData['contact_num'] = $poRecord['contact_num_1_phone'];
    $returnData['shop_status'] = $poRecord['status_opened_closed_display_name'];

    $returnData['products'] = $items;

    $returnData['collection_status'] = $poRecord['status'];

    // Set Return JSON Array Data
    $json_res_data["data"] = $returnData;
    $json_res_data["ack"] = 1;

    sleep(1);

}
else {
    $json_res_data["ack"] = 0;
    $deliveryAppLogger->error('Error: [Not $_POST]');
}

$deliveryAppLogger->info('returned json');
$deliveryAppLogger->info($json_res_data);
$deliveryAppLogger->info("[END PROCESS " . PROCESS . "]");

echo json_encode($json_res_data);
mysqli_close($dbConn);
return;

?>
