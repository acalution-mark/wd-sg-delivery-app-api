<?php

/**
 * DB Helper Class
 */
class DBHelper {

	private $logger;
	private $dbConn;

	/**
	 * Constructor
	 * 
	 * @param string $logger php logger
	 * @param string $dbConn DB Connection
	 *
	 * Here is an inline example:
	 * $dbhelper = new DBHelper($this->logger,$this->dbConn);
	 */
	public function __construct($logger, $dbConn){

		$this->logger = $logger;
		$this->dbConn = $dbConn;
	}

	/**
	 * A function to get a single database table row's all columns with a single where column.
	 * 
	 * @param string $table 	Database Table Name
	 * @param string $where 	SQL Where Column Name. Eg, id or state. Must be a valid Database name field
	 * @param string $datatype 	$where datatype. Either int or string
	 * @param string $value 	$where Column Value.
	 * @return array           	single row from SQL Select * Where Order By Order
	 * 
	 * Here is an inline example:
	 * $dbhelper = new DBHelper($this->logger,$this->dbConn);
	 * $table = 'products';
	 * $safe_product_id = mysqli_real_escape_string($this->dbConn, $product_id);
	 * $row = $dbhelper->getSingleRowWhereSingleColumn(
	 *									$table, // table name
	 *									"id", // where column name
	 *									"int", // where column datatype, int|str
	 *									$safe_product_id); // where column value
	 * if(!$row){
	 *	$this->logger->error("getSingleRowWhereSingleColumn [{$table}] Failed.");
	 *	return false;
	 * }
	 * foreach($row as $x => $x_value) {
	 *    $this->logger->debug("Key=[" . $x . "], Value=[" . $x_value . "]");
	 * }
	 * 
	 * $id = $row['id']; // To get column data
	 * return $row; // Return column data
	 */
	public function getSingleRowWhereSingleColumn($table,$where,$dataType,$value){

		$safe_table = mysqli_real_escape_string($this->dbConn, $table);
		$safe_where = mysqli_real_escape_string($this->dbConn, $where);
		$safe_dataType = mysqli_real_escape_string($this->dbConn, $dataType);
		$safe_value = mysqli_real_escape_string($this->dbConn, $value);

		$query  = "SELECT * ";
		$query .= "FROM {$safe_table} ";

		$query .= "WHERE {$safe_where}=";

		if($dataType == "int")
			$query .= "{$safe_value} ";
		else // string
			$query .= "'{$safe_value}' ";

		$query .= "LIMIT 1 ";

		$this->logger->debug('$query: [' . $query . "]");

		$result_set = mysqli_query($this->dbConn, $query);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return false;
		}

		if(!$row = mysqli_fetch_assoc($result_set)) {
  			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));
      
            return false;
        }

        return $row;

	}

	/**
	 * A function to get a multiple database table rows' all columns with a single where column.
	 * 
	 * @param  [type] $table      		Database Table Name
	 * @param  [type] $where 			SQL Where Column Name. Eg, id or state. Must be a valid Database name field
	 * @param  [type] $dataType   		$where datatype. Either int or string
	 * @param  [type] $value      		$where Column Value.
	 * @param  [type] $limitStart 		Limist Start
	 * @param  [type] $limitValue 		Limit Value. Eg, 1 or 10
	 * @param  [type] $orderByColumn    SQL Order By Column Name. Eg, id or name.
	 * @param  [type] $order      		SQL Order Direction. asc or desc
	 * @return array           			array of rows from SQL Select * Where Order By Order
	 *
	 * Here is an inline example:
	 * 
	 * $table = 'v_products_type_variety';
	 * $rows = $this->dbhelper->getMultipleRowsWhereSingleColumn(
	 * 		$table , // table
	 *		"state", // where column name
	 *		"str", // where column datatype int|str
	 *		$state, // where column value
	 *		NULL,  // Limit Start
	 *		NULL,  // Limit Value
	 *		"id", // Order By Column
	 *		"asc"); // Order Direction
	 *
	 * if(!$rows){
	 *		$this->logger->error("Get Table [{$table}] Row Columns Failed.");
	 *	 	return false;
	 * }
	 *	
	 * $rows_count = count($rows);
	 * $this->logger->debug("Returned rows count: [{$rows_count}]");
	 *
	 * $cnt = 1;
	 * $this->logger->debug("##Start FOREACH##");
	 * foreach($rows as $key => $row){
	 *
	 * 	$this->logger->debug("#New Row: [{$cnt}]#");
	 *
	 * 	$id = $row['id'];
	 *  
	 *  $cnt++;
	 *  
	 * }
	 * $this->logger->debug("##End FOREACH##");
	 */
	public function getMultipleRowsWhereSingleColumn($table,$where,$dataType,$value,$limitStart,$limitValue,$orderByColumn,$orderByDirection){

		$safe_table = mysqli_real_escape_string($this->dbConn, $table);
		$safe_where = mysqli_real_escape_string($this->dbConn, $where);
		$safe_dataType = mysqli_real_escape_string($this->dbConn, $dataType);
		$safe_value = mysqli_real_escape_string($this->dbConn, $value);
		$safe_limitStart = mysqli_real_escape_string($this->dbConn, $limitStart);
		$safe_limitValue = mysqli_real_escape_string($this->dbConn, $limitValue);
		$safe_orderByColumn = mysqli_real_escape_string($this->dbConn, $orderByColumn);
		$safe_orderByDirection = mysqli_real_escape_string($this->dbConn, $orderByDirection);

		$sql  = "SELECT * ";
		$sql .= "FROM {$safe_table} ";

		$sql .= "WHERE {$safe_where}=";

		if($dataType == "int")
			$sql .= "{$safe_value} ";
		else // string
			$sql .= "'{$safe_value}' ";

		if(strlen($safe_orderByColumn) > 0){

			$sql .= "Order By {$safe_orderByColumn} ";

			if(strtolower($safe_orderByDirection) == 'asc')
				$sql .= "{$safe_orderByDirection} "; // asc
			else if(strtolower($safe_orderByDirection) == 'desc'){
				$sql .= "{$safe_orderByDirection} "; // desc
			}
		}

		// $this->logger->debug("limitStart [$safe_limitStart] limitValue [$safe_limitValue]");

		if($safe_limitStart != NULL || $safe_limitValue != NULL){

			if($safe_limitStart == NULL){
				if($safe_limitValue > 0)
					$limit =  "LIMIT {$safe_limitValue} ";
			} else {
				if($safe_limitValue > 0)
					$limit = "LIMIT {$safe_limitStart}, {$safe_limitValue} ";
			}

			// $this->logger->debug("LIMIT [$limit]");
			$sql .= $limit;

		} else {
			// $this->logger->debug("LIMIT Start and Value are NULLs");
		}	
	
		$this->logger->debug('$sql: [' . $sql . ']');

		$result_set = mysqli_query($this->dbConn, $sql);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$safe_table}]. mysqli error: " . mysqli_error($this->dbConn));

			return false;
		}

		// Populate Return Array that will contain list and its data required to display in Frontend
		$list = array();

		$row_count = mysqli_num_rows($result_set);
		$this->logger->debug("mysqli_num_rows: [{$row_count}]");

		if($row_count < 1){

			$this->logger->warn("Row Count [{$row_count}]");
			return 0;
		}

		while($row = mysqli_fetch_assoc($result_set)){
			
			$list[] = $row;

		} // End of While Loop

		return $list;

	}

	/**
	 * [A function to get number of rows selected for a database table with a single where column.
	 * 
	 * @param  [type] $table    [description]
	 * @param  [type] $where    [description]
	 * @param  [type] $dataType [description]
	 * @param  [type] $value    [description]
	 * @return [type]           [description]
	 *
	 * Here is an inline example:
	 *
	 * $safe_user_id = mysqli_real_escape_string($this->dbConn, $user_id);
	 * $row_count = $this->dbhelper->getRowCountWhereSingleColumn(
	 *		"carts", // table name
	 *	 	"user_id", // where column name
	 *   	"str", // where column datatype, int|str
	 *    	$safe_user_id // where column value
	 * );
	 *   	
	 * if($row_count < 0){
	 * 		$this->logger->error("Get Cart Number of Products Row Count [{$row_count}] Failed.");
	 *	  	$return["ack"] = false;
	 *    	$return["msg"] = "Get Cart Number of Products Row Count [{$row_count}] Failed..";
	 *	    return $return;
	 * }
     *
	 * $this->logger->debug("Row Count [{$row_count}]");
	 */
	public function getRowCountWhereSingleColumn($table,$where,$dataType,$value){

		$safe_table = mysqli_real_escape_string($this->dbConn, $table);
		$safe_where = mysqli_real_escape_string($this->dbConn, $where);
		$safe_dataType = mysqli_real_escape_string($this->dbConn, $dataType);
		$safe_value = mysqli_real_escape_string($this->dbConn, $value);

		$query  = "SELECT * ";
		$query .= "FROM {$safe_table} ";

		$query .= "WHERE {$safe_where}=";

		if($dataType == "int")
			$query .= "{$safe_value} ";
		else // string
			$query .= "'{$safe_value}' ";

		// $this->logger->debug('$query: [' . $query . "]");

		$result_set = mysqli_query($this->dbConn, $query);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return -1;
		}

		$row_count = mysqli_num_rows($result_set);
		$this->logger->debug("Row Count [$row_count]");

        return $row_count;

	}

	/**
	 * [getRowCount description]
	 * @param  [type] $table               [description]
	 * @param  [type] $where_fields_values [description]
	 * @return [type]                      [description]
	 *
	 * Here is an inline example:
	 * $table = "carts";
	 * unset($where_array);
	 * $where_array = array(
	 * 		array( 
 	 *	 		"fieldname" => "product_id",
	 *	    	"bColons" => false,
	 *	     	"value" => $product_id),
	 *      array( 
	 *			"fieldname" => "user_id",
	 *	  		"bColons" => true,
	 *			"value" => $user_id)
	 * );
	 *
	 * $rows_count = $this->dbhelper->getRowsCount($table,$where_array);
	 *
	 * if($rows_count < 0){
	 *  	$this->logger->error("Get table [{$table}] Failed.");
	 * 		$return["ack"] = false;
	 * 		$return["msg"] = "Get table [{$table}] Failed.";
	 * 		return $return;
	 * }
	 * 
	 */
	public function getRowsCount($table,$where_fields_values){

		$sql = "SELECT * FROM {$table} WHERE ";

		for ($row = 0; $row < count($where_fields_values); $row++)
		{
		    $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");

		    $sql .= $where_fields_values[$row]["fieldname"]."=";

		    $escaped_value = mysqli_real_escape_string($this->dbConn, $where_fields_values[$row]["value"]);

		    if($where_fields_values[$row]["bColons"])
		    	// $sql .= "'". $where_fields_values[$row]["value"] . "'";
		    	$sql .= "'".  $escaped_value . "'";
		    else
	    		// $sql .= $where_fields_values[$row]["value"];
	    		$sql .=  $escaped_value;

	    	if($row + 1 < count($where_fields_values))
	    		$sql .= " AND ";
		} // End of For Loop

		$this->logger->debug("sql: [{$sql}]");

		$result = mysqli_query($this->dbConn, $sql);

		if (!$result) {
		    $err_msg = mysqli_error($this->dbConn);
		    $this->logger->error("Error getRowsCount: " . $err_msg);
		    return 0;
		}

		$rows_count = mysqli_num_rows($result);
		$this->logger->debug("Rows Count [$rows_count]");

        return $rows_count;

	}

	/**
	 * [getRows description]
	 * @param  [type] $table               [description]
	 * @param  [type] $where_fields_values [description]
	 * @param  [type] $limitStart          [description]
	 * @param  [type] $limitValue          [description]
	 * @param  [type] $orderByColumn       [description]
	 * @param  [type] $order               [description]
	 * @return [type]                      [description]
	 *
	 * $table = "carts";
	 * $where_array = array(
	 *		array( 
 	 *			"fieldname" => "product_id",
	 *		  	"bColons" => false,
	 *		   	"value" => $product_id),
	 *	    array( 
	 *	    	"fieldname" => "user_id",
	 *	     	"bColons" => true,
	 *		    "value" => $user_id)
	 * );
	 *
	 * $rows = $this->dbhelper->getRows($table,$where_array,NULL,NULL,NULL,NULL);
	 *
	 * if($rows < 0){
	 *  	$this->logger->error("Get table [{$table}] Failed.");
	 * 		$return["ack"] = false;
	 * 		$return["msg"] = "Get table [{$table}] Failed.";
	 * 		return $return;
	 * }
	 *
	 * $rows_count = count($rows);
	 * $this->logger->debug("Returned rows count: [{$rows_count}]");
	 *
	 * $list = array();
	 * $cnt = 1;
	 * 
	 * if($rows > 0) {
	 * $this->logger->debug("##Start FOREACH##");
	 * foreach($rows as $key => $row){
	 *  	$this->logger->debug("#New Row: [{$cnt}]#");
	 *
	 *		$id = $row['product_id'];
	 *
	 * 		$list[] = $row;
	 * 		
	 *		$cnt++;
 	 *	} // End of Loop
	 * 	$this->logger->debug("##End FOREACH##");
	 * 	}
	 */
	public function getRows($table,$where_fields_values,$limitStart,$limitValue,$orderByColumn,$order,$operator = "AND"){

		$this->logger->debug("Get Rows from table [$table]:");

		$sql = "SELECT * FROM {$table} WHERE ";

		for ($row = 0; $row < count($where_fields_values); $row++)
		{
		    
		    // $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");

		    if($where_fields_values[$row]["value"] == NULL){

		    	$sql .= $where_fields_values[$row]["fieldname"]." ";

		    	if($where_fields_values[$row]["bColons"])
			    	$sql .= "is NULL ";
			    else
		    		$sql .= "is not NULL ";

		    } else {

		    	$sql .= $where_fields_values[$row]["fieldname"]."=";

			    if($where_fields_values[$row]["bColons"])
			    	$sql .= "'". $where_fields_values[$row]["value"] . "' ";
			    else
		    		$sql .= $where_fields_values[$row]["value"] . " ";

		    }

	    	if($row + 1 <count($where_fields_values))
	    		$sql .= $operator . " ";

		} // End of For Loop

		if(strlen($orderByColumn) > 0){

			$sql .= "Order By {$orderByColumn} ";

			// $this->logger->debug("order: [{$order}]");

			if(strtolower($order) == 'desc'){
				$sql .= "{$order} "; // desc
			} else {
				$order = 'asc';
			}

		}

		// $this->logger->debug("limitStart [$limitStart] limitValue [$limitValue]");

		if($limitStart != NULL || $limitValue != NULL){

			if($limitStart == NULL){
				if($limitValue > 0)
					$limit =  "LIMIT {$limitValue} ";
			} else {
				if($limitValue > 0)
					$limit = "LIMIT {$limitStart}, {$limitValue} ";
			}

			// $this->logger->debug("LIMIT [$limit]");
			$sql .= $limit;

		} else {
			// $this->logger->debug("LIMIT Start and Value are NULLs");
		}
		
		$this->logger->debug('$sql: [' . $sql . ']');

		$result_set = mysqli_query($this->dbConn, $sql);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return -1;
		}

		// Populate Return Array that will contain list and its data required to display in Frontend
		$list = array();

		$row_count = mysqli_num_rows($result_set);
		$this->logger->debug("mysqli_num_rows: [{$row_count}]");

		if($row_count < 0){

			$this->logger->error("Row Count [{$row_count}]. Return");
			return -1;
		}

		if($row_count == 0){

			$this->logger->warn("Row Count [{$row_count}]. Return");
			return 0; // Return empty list

		}

		// $row_count > 0
		while($row = mysqli_fetch_assoc($result_set)){
			
			$list[] = $row;

		} // End of While Loop

		return $list;
	}

	/**
	 * [getAllRows description]
	 * @param  [type] $table               [description]
	 * @param  [type] $orderByColumn       [description]
	 * @param  [type] $order               [description]
	 * @return [type]                      [description]
	 *
	 * $table = 'country_dialing_codes';
	 * $rows = $dbhelper->getAllRows($table,"sequence_id");
	 *
	 * if($rows < 0){
	 *	$logger->error("Get table [{$table}] Failed.");
	 *
	 *	$json_res_data["ack"] = 1;
	 *	$json_res_data["err"] = 1;
	 *	$json_res_data["err_msg"] = "Get Table [{$table}] Row Details Failed.";
	 *
	 *	$logger->error($json_res_data);
	 *	$logger->info("[END PROCESS " . PROCESS . "]");
	 *
	 *	echo json_encode($json_res_data);
	 *	return;
	 * }
	 *
	 * $rows_count = count($rows);
	 * $logger->debug("Returned rows count: [{$rows_count}]");
	 *
	 * $list = array();
	 * $cnt = 1;
	 *
	 * $logger->debug("##Start FOREACH##");
	 * foreach($rows as $key => $row){
	 *	// $logger->debug("#New Row: [{$cnt}]#");
	 *
	 *	// $list[] = $row;
	 *
	 *	$dialing_code = $row['dialing_code'];
	 *	$country = $row['dialing_code'] . " " . $row['country'];
	 *
	 *	$logger->debug("[{$dialing_code}] [{$country}]");
	 *	
	 *	$list[] = array("id" => $dialing_code,"name" => $country);
	 *
 	 *	$cnt++;
 	 * } // End of Loop
  	 * $logger->debug("##End FOREACH##");
	 *
  	 * $json_res_data["data"] = $list;
	 * 	
	 */
	public function getAllRows($table,$orderByColumn = null,$order = null){

		$this->logger->debug("Get Rows from table [$table]:");

		$sql = "SELECT * FROM {$table} WHERE 1 ";
		
		if($orderByColumn != null){
			if(strlen($orderByColumn) > 0){
	
				$sql .= "Order By {$orderByColumn} ";
	
				// $this->logger->debug("order: [{$order}]");
	
				if(strtolower($order) == 'desc'){
					$sql .= "{$order} "; // desc
				} else {
					$order = 'asc';
				}
	
			}
		}

		$this->logger->debug('$sql: [' . $sql . ']');

		$result_set = mysqli_query($this->dbConn, $sql);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return -1;
		}

		// Populate Return Array that will contain list and its data required to display in Frontend
		$list = array();

		$row_count = mysqli_num_rows($result_set);
		$this->logger->debug("mysqli_num_rows: [{$row_count}]");

		if($row_count < 0){

			$this->logger->error("Row Count [{$row_count}]. Return");
			return -1;
		}

		if($row_count == 0){

			$this->logger->warn("Row Count [{$row_count}]. Return");
			return 0; // Return empty list

		}

		// $row_count > 0
		while($row = mysqli_fetch_assoc($result_set)){
			
			$list[] = $row;

		} // End of While Loop

		return $list;
	}
    
    public function getRowsWhereString($table,$select_columns,$where_string_values,$limitStart,$limitValue,$orderByColumn,$order){

		// $this->logger->debug("Get Rows from table [$table]:");

		$sql = "SELECT {$select_columns} FROM {$table} ";
        
        $sql .= "WHERE  {$where_string_values} ";

		if(strlen($orderByColumn) > 0){

			$sql .= "Order By {$orderByColumn} ";

			// $this->logger->debug("order: [{$order}]");

			if(strtolower($order) == 'desc'){
				$sql .= "{$order} "; // desc
			} else {
				$order = 'asc';
			}

		}

		// $this->logger->debug("limitStart [$limitStart] limitValue [$limitValue]");

		if($limitStart != NULL || $limitValue != NULL){

			if($limitStart == NULL){
				if($limitValue > 0)
					$limit =  "LIMIT {$limitValue} ";
			} else {
				if($limitValue > 0)
					$limit = "LIMIT {$limitStart}, {$limitValue} ";
			}

			// $this->logger->debug("LIMIT [$limit]");
			$sql .= $limit;

		} else {
			// $this->logger->debug("LIMIT Start and Value are NULLs");
		}
		
		// $this->logger->debug('$sql: [' . $sql . ']');

		$result_set = mysqli_query($this->dbConn, $sql);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return -1;
		}

		// Populate Return Array that will contain list and its data required to display in Frontend
		$list = array();

		$row_count = mysqli_num_rows($result_set);
		// $this->logger->debug("mysqli_num_rows: [{$row_count}]");

		if($row_count < 0){

			$this->logger->error("Row Count [{$row_count}]. Return");
			return -1;
		}

		if($row_count == 0){

			$this->logger->warn("Row Count [{$row_count}]. Return");
			return 0; // Return empty list

		}

		// $row_count > 0
		while($row = mysqli_fetch_assoc($result_set)){
			
			$list[] = $row;

		} // End of While Loop

		return $list;
	}

	/**
	 * [getDistinctRows description]
	 * @param  [type] $table               [description]
	 * @param  [type] $select_fields       [description]
	 * @param  [type] $where_fields_values [description]
	 * @param  [type] $limitValue          [description]
	 * @param  [type] $orderByColumn       [description]
	 * @param  [type] $order               [description]
	 * @return [type]                      [description]
	 * 
	 * inline example:
	 *
	 * $dbhelper = new DBHelper($searchbarLogger,$dbConn);
	 *
	 * $table = "v_products_published_only";
	 * $select_array = array("type","type_display_name");
	 *
	 * $where_array = array(
	 * 		array( 
 	 *			"fieldname" => "type",
	 *	   		"bColons" => true,
	 *		    "value" => $type)
	 * );	
	 * 
	 * $rows = $dbhelper->getDistinctRows($table,$select_array,$where_array,NULL,NULL,NULL);
 	 * 
	 * if(!$rows < 0){
	 *   	$searchbarLogger->error("Get table [{$table}] Failed.");
	 *	$return["ack"] = false;	
	 *	$return["msg"] = "Get table [{$table}] Failed.";
	 *	return $return;
	 *}
	 * 
	 * $rows_count = count($rows);
	 * $searchbarLogger->debug("Returned rows count: [{$rows_count}]");
	 *
	 * $winetype = array();
	 * $cnt = 1;
	 *
	 * $searchbarLogger->debug("##Start FOREACH##");
	 * foreach($rows as $key => $row){
	 * 		$searchbarLogger->debug("#New Row: [{$cnt}]#");
 	 *
	 * 		$type = $row['type'];
	 *   	$searchbarLogger->debug("type: [{$type }]");
	 *    	$type_display_name = $row['type_display_name'];
	 *     	$searchbarLogger->debug("type: [{$type_display_name}]"); 
	 *      $winetype[$type] = $type_display_name ;
	 *
	 * 		$cnt++;
	 * } // End of Loop
	 * $searchbarLogger->debug("##End FOREACH##");
	 */
	public function getDistinctRows($table,$select_fields,$where_fields_values,$limitValue,$orderByColumn,$order){

		// $this->logger->debug("START function getDistinctRows");

		$sql_select = "";

		foreach($select_fields as $key => $row){

			// $this->logger->debug("fieldname [".$row."]");
			$sql_select .= $row . ",";
		
		}

		$sql_select = rtrim($sql_select, ",");
		$sql_select = "DISTINCT " . $sql_select . " ";
		// $this->logger->debug("sql_select: [{$sql_select}]");

		$sql = "SELECT " . $sql_select . " FROM {$table} WHERE ";

		if(count($where_fields_values) < 1){ // No Where Fields provided. Set Where 1
			$sql .= " 1 ";
		} else {
			for ($row = 0; $row < count($where_fields_values); $row++)
			{
			    
			    // $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");

			    $sql .= $where_fields_values[$row]["fieldname"]."=";

			    if($where_fields_values[$row]["bColons"])
			    	$sql .= "'". $where_fields_values[$row]["value"] . "' ";
			    else
		    		$sql .= $where_fields_values[$row]["value"] . " ";

		    	if($row + 1 <count($where_fields_values))
		    		$sql .= " AND ";

			} // End of For Loop
		}

		if($limitValue > 0)
			$sql .= " LIMIT {$limitValue} ";

		if(strlen($orderByColumn) > 0){

			$sql .= " Order By {$orderByColumn} ";

			$order = 'asc';

			if(strtolower($order) == 'desc'){
				$sql .= " {$order} "; // desc
			} 

		}

		$this->logger->debug('$sql: [' . $sql . ']');

		$result_set = mysqli_query($this->dbConn, $sql);
		if(!$result_set){
			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));

			return -1;
		}

		// Populate Return Array that will contain list and its data required to display in Frontend
		$list = array();

		$row_count = mysqli_num_rows($result_set);
		$this->logger->debug("mysqli_num_rows: [{$row_count}]");

		if($row_count < 0){

			$this->logger->error("Row Count [{$row_count}]");
			return -1;
		}

		if($row_count == 0){
			$this->logger->error("Row Count is 0");
			return $list; // Return empty list

		}

		// $row_count > 0
		while($row = mysqli_fetch_assoc($result_set)){
			
			$list[] = $row;

		} // End of While Loop

		return $list;

	}

	/**
	 * [insertRow description]
	 * @param  [type] $table         [description]
	 * @param  [type] $fields_values [description]
	 * @return [type]                [description]
	 *
	 * $date = date('Y-m-d H:i:s');
	 * $table = "carts";
	 * 
	 * $array = array(
	 * 				array( 
	 * 	   				"fieldname" => "created_at",
	 * 					"bColons" => true,
	 * 					"value" => $date),
	 * 				array( 
	 * 					"fieldname" => "created_by",
	 * 					"bColons" => true,
	 * 					"value" => $name),
	 * 				array( 
	 * 					"fieldname" => "bGuest",
	 * 					"bColons" => false,
	 * 					"value" => $bLoggedIn),
	 *				array( 
	 *					"fieldname" => "user_id",
	 *					"bColons" => true,
	 *					"value" => $user_id),
	 *				array( 
	 *					"fieldname" => "product_id",
	 *					"bColons" => false,
	 *					"value" => $product_id),
	 *				array( 
	 *					"fieldname" => "cart_product_last_datetime",
	 *					"bColons" => true,
	 *					"value" => $date),
	 *				array( 
	 *					"fieldname" => "qty",
	 *					"bColons" => false,
	 *					"value" => $quantity),
	 *				array( 
	 *					"fieldname" => "price",
	 *					"bColons" => true,
	 *					"value" => $price),
	 *				array( 
	 *					"fieldname" => "discount",
	 *					"bColons" => true,
	 *					"value" => $price_amt_discounted),
	 *				array( 
	 *					"fieldname" => "price_with_discount",
	 *					"bColons" => true,
	 *					"value" => $price_after_discounts)
	 *				);
	 *
	 * 
	 * $id = $this->dbhelper->insertRow($table ,$array);
     *
     * if($id == false){
     *	  	$this->logger->error("Insert to Table [{$table}] failed.");
     *	  	$return["ack"] = false;
     *		$return["msg"] = "Insert to Table [{$table}] failed.";
     *		return $return;
     *	}
	 *
	 * $this->logger->info("Insert to Table [{$table}] ID [{$id}]");
	 * 
	 */
	public function insertRow($table,$fields_values){

		$insert_sql_columns = "";
        $insert_sql_values = "";

		for ($row = 0; $row < count($fields_values); $row++)
		{
		    // $this->logger->debug("fieldname [".$fields_values[$row]["fieldname"]."] colons [".$fields_values[$row]["bColons"]."] value [".$fields_values[$row]["value"] . "]");
		
            $insert_sql_columns .= ", {$fields_values[$row]["fieldname"]} ";

            $escaped_value = mysqli_real_escape_string($this->dbConn, $fields_values[$row]["value"]);
            // $this->logger->debug("escaped_value: [{$escaped_value}]");
            
            if($fields_values[$row]["bColons"])
            	$insert_sql_values .= ", '{$escaped_value}'";
            else
            	$insert_sql_values .= ", {$escaped_value}";      
		}

		$insert_sql_columns= ltrim ($insert_sql_columns, ',');
		$insert_sql_values= ltrim ($insert_sql_values, ',');

		$sql = "INSERT INTO {$table} (";
        $sql .= $insert_sql_columns;
        $sql .= ") VALUES (";
        $sql .= $insert_sql_values;
        $sql .= ")";
                             
        $this->logger->debug("sql: [{$sql}]");

        $result = mysqli_query($this->dbConn,$sql);
        if ($result) { // Success 

        	$id = mysqli_insert_id($this->dbConn);
        	$this->logger->debug("SQL Successful");

        	return $id;

        } else {

        	$this->logger->error("mysqli error [ " . mysqli_error($this->dbConn) . "]");
        }

		return false;

	}

	/**
	 * [updateRow description]
	 * @param  [type] $table               [description]
	 * @param  [type] $set_fields_values   [description]
	 * @param  [type] $where_fields_values [description]
	 * @return [type]                      [description]
	 *
	 * $date = date('Y-m-d H:i:s');
	 * 
	 * $table = "carts";
	 * 
	 * $set_fields_values = array(
	 *	   array( 
	 * 	  		"fieldname" => "cart_product_last_datetime",
	 * 	   		"bColons" => true,
	 * 	     	"value" => $date),
	 * 	    array( 
	 * 	    	"fieldname" => "qty",
	 *	      	"bColons" => false,
	 *	       	"value" => $new_qty)
	 * );
	 * 
	 * $where_fields_values = array(
     * 		array( 
     * 			"fieldname" => "product_id",
     * 	 		"bColons" => false,
     * 	  		"value" => $product_id),
     * 	   array( 
     * 		  	"fieldname" => "user_id",
     *     		"bColons" => true,
     *       	"value" => $user_id)
     * );
	 * 
	 * $affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);
	 * 		
	 * if($affected_num_rows < 0){
	 * 		$this->logger->error("Update [{$table}] Failed.");
	 *	 	$return["ack"] = false;
	 *	 	$return["msg"] = "Update [{$table}] Failed.";
	 *	 	return $return;
	 * }
	 */
	public function updateRow($table,$set_fields_values,$where_fields_values){

		unset($errors);

		$sql = "UPDATE {$table} SET ";

		for ($row = 0; $row < count($set_fields_values); $row++)
		{
		    // $this->logger->debug("fieldname [".$set_fields_values[$row]["fieldname"]."] colons [".$set_fields_values[$row]["bColons"]."] value [".$set_fields_values[$row]["value"] . "]");
		
		    $sql .= $set_fields_values[$row]["fieldname"] . "=";

		    $escaped_value = mysqli_real_escape_string($this->dbConn, $set_fields_values[$row]["value"]);

            if($set_fields_values[$row]["bColons"])
            	$sql .= "'". $escaped_value ."' ";
            else {
            	
            	if($set_fields_values[$row]["value"] == "NULL")
            		$sql .= "NULL" ." ";
            	else
            		$sql .= $escaped_value ." ";
            }

            if($row + 1 <count($set_fields_values))
	    		$sql .= ", ";

		} // End of For Loop

		if(count($where_fields_values) < 1){

			$sql .= "WHERE 1 ";

		} else {
			
			$sql .= "WHERE ";

			for ($row = 0; $row < count($where_fields_values); $row++)
			{
			    // $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");
			
			    $sql .= $where_fields_values[$row]["fieldname"] . "=";
			    $escaped_value = mysqli_real_escape_string($this->dbConn, $where_fields_values[$row]["value"]);
			    
	            if($where_fields_values[$row]["bColons"])
	            	$sql .= "'". $escaped_value ."' ";
	            else
	            	$sql .= $escaped_value ." ";

	            if($row + 1 <count($where_fields_values))
		    		$sql .= "AND ";

			} // End of For Loop
		
		}

		$this->logger->debug("sql: [{$sql}]");

        $result = mysqli_query($this->dbConn,$sql);
        if ($result) { // Success 

        	$affected_num_rows = mysqli_affected_rows($this->dbConn);
        	$this->logger->debug("Affected rows (UPDATE): [{$affected_num_rows}]");
        	$this->logger->debug("SQL Successful");

        	return $affected_num_rows;

        } else {

        	$this->logger->error("mysqli error [ " . mysqli_error($this->dbConn) . "]");
        
        	$errors[] = "mysqli error [ " . mysqli_error($this->dbConn) . "]";
        	return -1;
        }

		return -1;

	}

	/**
	 * [updateRowAddSubstractNumber description]
	 * @param  [type] $table               [description]
	 * @param  [type] $set_fields_values   [description]
	 * @param  [type] $where_fields_values [description]
	 * @return [type]                      [description]
	 *
	 *	Inline Example:
	 * $table = "products";
	 *
	 * $set_fields_values = array(
	 *  	array( 
	 *		 	"fieldname" => "quantity_reserved",
	 *		  	"operation" => '+',
	 *		   	"value" => $cart_item['qty']),
	 *      array( 
	 *		    "fieldname" => "quantity_not_reserved",
	 *		    "operation" => '-',
	 *		    "value" => $cart_item['qty'])
	 * );
	 *
  	 * $where_fields_values = array(
     *		array( 
     *			"fieldname" => "id",
     *		 	"bColons" => false,
     *		  	"value" => $cart_item['product_id'])
     * );
	 *
	 * $product = $dbhelper->getRows($table,$where_fields_values,NULL,1,NULL,NULL);
	 *
	 *	if(!$product < 0){
	 *		$this->logger->error("Get table [{$table}] Failed.");
	 *		$return["ack"] = false;
	 *		$return["msg"] = "Get table [{$table}] Failed.";
	 *		return $return;
	 *	}
	 *
	 *	$product_rows_count = count($product);
	 *	$logger->debug("Returned product rows count: [{$product_rows_count}]");
	 *
     *	$affected_num_rows = $dbhelper->updateRowAddSubstractNumber($table,$set_fields_values,$where_fields_values);
 	 *	if($affected_num_rows < 0){
	 * 		$logger->error("Update [{$table}] Failed.");
	 *		$return["ack"] = false;
	 *		$return["msg"] = "Update [{$table}] Failed.";
	 *		return $return;
	 *	}
	 *
	 *	$logger->debug("Update product: product_id [{$cart_item['product_id']}] qty [{cart_item['qty']}]");
	 */
	public function updateRowAddSubstractNumber($table,$set_fields_values,$where_fields_values){

		$sql = "UPDATE {$table} SET ";

		for ($row = 0; $row < count($set_fields_values); $row++)
		{
		    // $this->logger->debug("fieldname [".$set_fields_values[$row]["fieldname"]."] colons [".$set_fields_values[$row]["operation"]."] value [".$set_fields_values[$row]["value"] . "]");
		
		    $sql .= $set_fields_values[$row]["fieldname"] . "=";

            if($set_fields_values[$row]["operation"] == '+')
            	$sql .= $set_fields_values[$row]["fieldname"] . "+" . $set_fields_values[$row]["value"] . " ";
            else
            	$sql .= $set_fields_values[$row]["fieldname"] . "-" . $set_fields_values[$row]["value"] . " ";

            if($row + 1 <count($set_fields_values))
	    		$sql .= ", ";

		} // End of For Loop

		if(count($where_fields_values) < 1){

			$sql .= "WHERE 1 ";

		} else {
			
			$sql .= "WHERE ";

			for ($row = 0; $row < count($where_fields_values); $row++)
			{
			    $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");
			
			    $sql .= $where_fields_values[$row]["fieldname"] . "=";
	            if($where_fields_values[$row]["bColons"])
	            	$sql .= "'". $where_fields_values[$row]["value"] ."' ";
	            else
	            	$sql .= $where_fields_values[$row]["value"] ." ";

	            if($row + 1 <count($where_fields_values))
		    		$sql .= "AND ";

			} // End of For Loop
		
		}

		$this->logger->debug("sql: [{$sql}]");

        $result = mysqli_query($this->dbConn,$sql);
        if ($result) { // Success 

        	$affected_num_rows = mysqli_affected_rows($this->dbConn);
        	$this->logger->debug("Affected rows (UPDATE): [{$affected_num_rows}]");
        	$this->logger->debug("SQL Successful");

        	return $affected_num_rows;

        } else {

    		$this->logger->error("mysqli error [ " . mysqli_error($this->dbConn) . "]");
        }

		return -1;

	}

	/**
	 * [delete description]
	 * @param  [type] $table               [description]
	 * @param  [type] $where_fields_values [description]
	 * @return [type]                      [description]
	 *
	 * $dbhelper = new DBHelper($this->logger,$this->dbConn);
	 * $where_fields_values = array(
  	 *				array( 
  	 *					"fieldname" => "user_id",
  	 *					"bColons" => false,
  	 *					"value" => $user_id),
  	 *				array( 
  	 *					"fieldname" => "product_id",
  	 *					"bColons" => false,
  	 *					"value" => $product_id)
  	 *			);
  	 *
  	 * $table = "carts";
	 *	$ret = $this->dbhelper->delete($table,$where_fields_values);
	 * if(!$ret){
	 *		$this->logger->error("Delete Error for Table [{$table}]");
	 *		return false;
	 *	}
	 * 
	 */
	public function delete($table,$where_fields_values){

		$sql = "DELETE FROM {$table} WHERE ";

		for ($row = 0; $row < count($where_fields_values); $row++)
		{
		    
		    $this->logger->debug("fieldname [".$where_fields_values[$row]["fieldname"]."] colons [".$where_fields_values[$row]["bColons"]."] value [".$where_fields_values[$row]["value"] . "]");

		    $sql .= $where_fields_values[$row]["fieldname"]."=";

		    if($where_fields_values[$row]["bColons"])
		    	$sql .= "'". $where_fields_values[$row]["value"] . "'";
		    else
	    		$sql .= $where_fields_values[$row]["value"];

	    	if($row + 1 <count($where_fields_values))
	    		$sql .= " AND ";

		} // End of For Loop

		$this->logger->debug("sql: [{$sql}]");

		if (!mysqli_query($this->dbConn, $sql)) {
		    $err_msg = mysqli_error($this->dbConn);
		    $this->logger->error("Error deleting record: " . $err_msg);
		    return false;
		}

		$this->logger->debug("Affected rows (DELETE): " . mysqli_affected_rows($this->dbConn) );
	    $this->logger->debug("Record(s) deleted successfully");

		return true;

	}

}

?>
