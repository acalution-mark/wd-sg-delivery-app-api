<?php 

class Collection
{

    private $logger;
    private $dbConn;

    private $dbhelper;

    public function __construct($logger, $dbConn){

        $this->logger = $logger;
        $this->dbConn = $dbConn;

        $this->dbhelper = new DBHelper($this->logger,$this->dbConn);

    }

    public function create($data){

    } // End of public function create($data)

    public function update($data){


        $this->logger->debug("Start: Collection Class update fn:");
        $this->logger->debug($data);

        $this->logger->debug("user_id: [{$data['user_id']}] user_display_name: [{$data['user_display_name']}]");

        // Get Key Column Value
        if(isset($data['status']) && exists($data['status'])){

        }
        else {
            $this->logger->error("Missing status.");
            $json_res_data["ack"] = 1;
            $json_res_data["err"] = 1;
            $json_res_data["err_msg"] = "Missing status.";

            return $json_res_data;
        }

        $table = "merchants_orders";

        $where_array = array();

        $where_array[] = array( 
            "fieldname" => "purchase_order_num",
            "bColons" => true,
            "value" => $data['purchase_order_num']);

        $rows_count = $this->dbhelper->getRowsCount($table,$where_array);

        if($rows_count < 0){
            $this->logger->error("getRowsCount table [{$table}] Failed.");
            $json_res_data["ack"] = 1;
            $json_res_data["err"] = 1;
            $json_res_data["err_msg"] = "getRowsCount table [{$table}] Failed.";
            return $json_res_data;
        }

        if($rows_count > 0){

        }


        // Reach here means there were no errors

        // For Update for Table [users_merchants]
        $set_fields_values = array();

        if(isset($data['status']) && exists($data['status'])){
            $set_fields_values[] = array( 
                "fieldname" => "status",
                "bColons" => true,
                "value" => $data['status']);
        }
        
        if(count($set_fields_values) > 0){
            
            $table = "merchants_orders";
            
            $set_fields_values[] = array( 
                "fieldname" => "updated_at",
                "bColons" => false,
                "value" => 'NOW()');

            $set_fields_values[] = array( 
                "fieldname" => "updated_by",
                "bColons" => true,
                "value" => $data['user_display_name']);

            // SQL Where
            $where_fields_values = array(
                array( 
                    "fieldname" => "purchase_order_num",
                    "bColons" => true,
                    "value" => $data['purchase_order_num'])
            );

            $affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);

            if($affected_num_rows < 0){

                $this->logger->warn('Update [{$table}] Failed.');
                $json_res_data["ack"] = 1;
                $json_res_data["err"] = 1;
                $json_res_data["err_msg"] = 'Update [{$table}] Failed.';

                return $json_res_data;
            }

        } else {

            $this->logger->info("No fields values to update. Table [{$table}] User ID [{$user_id}]");

        }

        
        // END: For Update for Table [merchants_orders]

    } // End of public function update($data){

    public function updateList($data){


        $this->logger->debug("Start: Collection Class updateList fn:");

        foreach($data['data'] as $key => $value){
            $this->logger->debug("purchase_order_num: [{$value['purchase_order_num']}] status: [{$value['status']}] user_id: [{$value['user_id']}] user_display_name: [{$value['user_display_name']}]");
            
            // Get Key Column Value
            if(isset($value['status']) && exists($value['status'])){
            
            }
            else {
                $this->logger->error("Missing status.");
                $this->logger->error("Continuing to next array.");

                continue;
            }
            
            $table = "merchants_orders";

            $where_array = array();

            $where_array[] = array( 
                "fieldname" => "purchase_order_num",
                "bColons" => true,
                "value" => $value['purchase_order_num']);

            $rows_count = $this->dbhelper->getRowsCount($table,$where_array);

            if($rows_count < 0){
                $this->logger->error("getRowsCount table [{$table}] Failed.");
                $this->logger->error("Continuing to next array.");
                
                continue;
            }
            
            // Reach here means there were no errors
            
            // For Update for Table [merchants_orders]
            $set_fields_values = array();

            if(isset($value['status']) && exists($value['status'])){
                $set_fields_values[] = array( 
                    "fieldname" => "status",
                    "bColons" => true,
                    "value" => $value['status']);
            }
            
            if(count($set_fields_values) > 0){
                
                unset($table);
                unset($where_fields_values);
                
                $table = "merchants_orders";

                $set_fields_values[] = array( 
                    "fieldname" => "updated_at",
                    "bColons" => false,
                    "value" => 'NOW()');

                $set_fields_values[] = array( 
                    "fieldname" => "updated_by",
                    "bColons" => true,
                    "value" => $value['user_display_name']);

                // SQL Where
                $where_fields_values = array(
                    array( 
                        "fieldname" => "purchase_order_num",
                        "bColons" => true,
                        "value" => $value['purchase_order_num'])
                );

                $affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);
                
                if($affected_num_rows < 0){
                    $this->logger->warn('Update [{$table}] Failed.');
                }
            }
            else {

                $this->logger->info("No fields values to update. Table [{$table}] User ID [{$user_id}]");

            }
        }
        // END: foreach($data['data'] as $key => $value){

    } // End of public function updateList($data){

    public function delete($data){

    } // End of public function delete($data){

}

?>