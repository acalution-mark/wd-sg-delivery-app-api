<?php 

class FileUploadTemp
{

	private $logger;
	private $dbConn;

	private $dbhelper;

	public function __construct($logger, $dbConn){

		$this->logger = $logger;
		$this->dbConn = $dbConn;

		$this->dbhelper = new DBHelper($this->logger,$this->dbConn);

	}

	public function create($data, $files){

		$this->logger->debug("Start: FileUploadTemp Class create fn: ");

		$json_res_data = array();
		if(isset($files['name'])){

			$filesCount = count($files['name']);
			$this->logger->debug("Number of files to upload [" . $filesCount . "]");

			if($filesCount > 0){

				$this->logger->debug("There are [" . $filesCount . "] files to upload");

				$http_saved_dir = "/uploaded_files/temp/";
				$target_dir = "/var/www/default/public_html/uploaded_files/temp/";

				$table = "files_upload_temp";
				$date = date('Y-m-d H:i:s');

				for ($x = 0; $x < $filesCount; $x++) {
				    $this->logger->debug("name: [" . $files['name'][$x] . "] type [" . $files['type'][$x] . "]");
				
					unset($fields_values);

				    $fields_values[] = array( 
						"fieldname" => "created_at",
						"bColons" => true,
						"value" => $date);

					$fields_values[] = array( 
						"fieldname" => "created_by",
						"bColons" => true,
						"value" => __FILE__);

		 			$filename = str_replace(' ', '_', basename($files['name'][$x]));
					$http_saved_dir_file = $http_saved_dir . "_" . time() . "_filename_" . $filename;

		 			$fields_values[] = array( 
						"fieldname" => "uploaded_filepath",
						"bColons" => true,
						"value" => $http_saved_dir_file);

		 			$fields_values[] = array( 
						"fieldname" => "uploaded_file_name",
						"bColons" => true,
						"value" => $files['name'][$x]);
					
			 		$inserted_id = $this->dbhelper->insertRow($table,$fields_values);

				 	if($inserted_id == false){
				 		$this->logger->error("Insert to Table [{$table}] failed.");

				  		$json_res_data["ack"] = 1;
						$json_res_data["err"] = 1;
						$json_res_data["err_msg"] = "Insert to Table [{$table}] failed.";

						return;
					}

					// Set Return Variables
					$json_res_data["row_id"] = $inserted_id;
	 				$json_res_data["uploaded_file_name"] = $files['name'][$x];

					$move_dir_file = $target_dir . "rowid_" . $inserted_id . "_" . time() . "_filename_" . $filename;
					$http_saved_dir_file = $http_saved_dir . "rowid_" . $inserted_id . "_" . time() . "_filename_" . $filename;

					if (move_uploaded_file($files['tmp_name'][$x], $move_dir_file)) {

						$this->logger->info('$move_dir_file: [' . $move_dir_file . ']');

						$table = "files_upload_temp";
						
						$set_fields_values = array(
					 	   array( 
					  	  		"fieldname" => "uploaded_filepath",
					  	   		"bColons" => true,
					 	     	"value" => $http_saved_dir_file));
						 
						$where_fields_values = array(
					    	array( 
					  	  		"fieldname" => "id",
					  	   		"bColons" => false,
					 	     	"value" => $inserted_id));
						
						$affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);
								
						if($affected_num_rows < 0){
							$this->logger->error("Update [{$table}] Failed.");
						 	$return["ack"] = false;
						  	$return["msg"] = "Update [{$table}] Failed.";
						 	return $return;
					 	}

					 	// Set Return Variables
					 	$json_res_data["uploaded_filepath"] = $http_saved_dir_file;

					} // if (move_uploaded_file($files['tmp_name'][$x], $move_dir_file)) {
					else {

						$this->logger->error("There was an error saving file.");
						$json_res_data["ack"] = 1;
						$json_res_data["err"] = 1;
						$json_res_data["err_msg"] = "There was an error saving file.";
						return $json_res_data;

					}

				} // for ($x = 0; $x < count($files); $x++) {

			} else {
				$this->logger->debug("There are no files to upload");
			}

		} else {
			$this->logger->debug("There are no files to upload");
		}

		// Set Return Variables
	 	$json_res_data["err"] = 0;

	 	$this->logger->debug("End: FileUploadTemp Class create fn: ");

	 	return $json_res_data;

	}

}

?>
