<?PHP

class PDF_Invoice1 extends FPDF
{

	private $logger;
	private $dbConn;

	private $dbhelper;

	// Page header
	function Header()
	{
	    // Logo
	    // $this->Image('letterhead_fhi_logo.png',10,6,30);
	    $this->Image('/var/www/default/public_html/invoices/letterhead_fhi_logo.png',10,10,20);
	    $x=110;
		$y=10; 
		$this->setXY($x,$y);
	    $this->SetFont('Arial','',10);
	    $this->Cell(0,5,'Fitness And Health International Pte Ltd',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'6 Ubi Road, #04-10, Wintech Centre',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'Singapore 408726',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'Tel: (65) 6842 4577',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'Email: info@fhi-online.com',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'Website: www.fhi-online.com',0,1,'R');
	    $y=$y+(5); 
	    $this->setXY($x,$y);
	    $this->Cell(0,5,'Co. Reg No.: 200104257R',0,1,'R');

	    $this->Ln(5);

	    $this->setXY(10,$y+20);
	}

	// Page footer
	function Footer()
	{
		$this->SetFont('Arial','',8);
		$this->Ln(5);
	    $this->Cell(0,10,'*This is a computer-generated invoice and no signature is required',0,1,'L');
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}

	// table
	function SalesTable($header, $data, $data_summary)
	{

	    // Colors, line width and bold font
	    $this->SetFillColor(224,224,224);
	    $this->SetTextColor(0);
	    $this->SetDrawColor(0,0,0);
	    $this->SetLineWidth(.3);
	    $this->SetFont('','B');
	    // Header
	    $w = array(100,30,30,30); // Header Width Columns: Item Name, Qty, Rate, amount
	    for($i=0;$i<count($header);$i++)
	        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	    $this->Ln();
	    // Data Rows
	    $this->SetFont('Arial','');
	    foreach($data as $row)
	    {
	    	// Item Name
	    	if(strlen($row[0]) > 50){ // Need to set max length to 50 chars to pdf print nicely out in cell as can't auto word wrap in cell.
	        	$item_name = substr($row[0],0,50);
	        	// echo 'str: [' . $item_name . '] len: ' . strlen($item_name) . "\n";
	        } else{
	        	$item_name = $row[0];
	        }
	        $this->Cell($w[0],6,$item_name,1,0,'L');

	        // Qty
	        $this->Cell($w[1],6,$row[1],1,0,'C');

	        // Rate
	        $this->Cell($w[2],6,$row[2],1,0,'C');

	        // Amount
	        $this->Cell($w[3],6,$row[3],1,0,'C');
	        $this->Ln();
	    }
     	$this->SetFillColor(224,224,224);
	    
	    $this->Cell(160,6,'SubTotal: $',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['subtotal'],1,0,'C',true);
	    $this->Ln();
	    $this->Cell(160,6,'Total Price Payable (Includes GST): $',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['total_price'],1,0,'C',true);
	    $this->Ln();
	    $this->Cell(160,6,'Life Points Spent: ',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['life_points_spent'],1,0,'C',true);
	    $this->Ln();
	    $this->Cell(160,6,'D-Cash Spent: ',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['d_cash_spent'],1,0,'C',true);
	    $this->Ln();
	    $this->Cell(160,6,'Credit Card Payment: $',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['credit_cart_payment'],1,0,'C',true);
	    $this->Ln();
	    $this->Cell(160,6,'Life Points Earned',1,0,'R',true);
	    $this->Cell(30,6,$data_summary['life_points_earned'],1,0,'C',true);
	    $this->Ln();

	}

}

?>