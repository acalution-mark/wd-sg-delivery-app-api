<?php

class User 
{

	private $logger;
	private $dbConn;

	private $dbhelper;

	private $http_path;

	public function __construct($logger, $dbConn){

		$this->logger = $logger;
		$this->dbConn = $dbConn;

		$this->dbhelper = new DBHelper($this->logger,$this->dbConn);

		// Get Server Settings HTTP_PATH
	  	$row = $this->dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"http_path"); // where column value
		
		if(!$row){
	 		$this->logger->error("getRowsCount [{$table}] Failed.");
	 		return false;
	  	}

	  	$this->http_path = $row['value']; // To get column data

	  	// $this->logger->debug("http_path [{$http_path}]");
	}

	private function generateSalt($length) {
      	// Not 100% unique, not 100% random, but good enough for a salt
      	// MD5 returns 32 characters
      	$unique_random_string = md5(uniqid(mt_rand(), true));
      
    	// Valid characters for a salt are [a-zA-Z0-9./]
      	$base64_string = base64_encode($unique_random_string);
      
        // But not '+' which is valid in base64 encoding
      	$modified_base64_string = str_replace('+', '.', $base64_string);
      
        // Truncate string to the correct length
      	$salt = substr($modified_base64_string, 0, $length);
      
        return $salt;
    }    

	public function passwordEncrypt($password) {
    	$hash_format = "$2y$10$";   // Tells PHP to use Blowfish with a "cost" of 10
      	$salt_length = 22;          // Blowfish salts should be 22-characters or more
      	$salt = $this->generateSalt($salt_length);
      	$format_and_salt = $hash_format . $salt;
      	$hash = crypt($password, $format_and_salt);
        return $hash;
    }

	private function passwordCheck($password, $existing_hash){
		
		$this->logger->debug('class Login passwordCheck()');
		
		$hash = crypt($password, $existing_hash);
		if ($hash === $existing_hash) {
	        return true;
		} else {
			return false;
		}		
	} // End of private function passwordCheck($password, $existing_hash){
	
    private function getUserTypeDbTable($user_type){

    	if($user_type == "account"){
    		$table = "users_admin";
    	}
    	else {
    		// $table = false;
    	}

    	return $table;
    }

    public function login($user_type,$username,$password){
		
		$this->logger->debug('Class User login()');
		
		$table = $this->getUserTypeDbTable($user_type);
		$this->logger->debug("user_type: [{$user_type}] table: [{$table}]");

		if($table != false){ // if not false

			// remove all whitespaces
			// See http://stackoverflow.com/questions/2109325/how-to-strip-all-spaces-out-of-a-string-in-php
			$username = preg_replace('/\s+/', '', $username);

	 		$record = $this->dbhelper->getSingleRowWhereSingleColumn(
				$table, // table name
				"username", // where column name
				"str", // where column datatype, int|str
				$username); // where column value

	 		if(!$record){
				$this->logger->error("getSingleRowWhereSingleColumn [{$table}] Failed.");
				return false;
			}

 			$this->logger->debug("user_type [{$user_type}] record type [{$record['type']}] has_deli_rights [{$record['has_deli_rights']}]");

            if($record['has_deli_rights'] != 1){
				// user does not have rights to use delivery app
                $this->logger->debug("user does not have rights to use delivery app");
                return false;
			}
            
 			if($user_type != $record['type']){
				// user type does not match
                $this->logger->debug("user type does not match");
                return false;
			}

			if ($this->passwordCheck($password, $record["hashed_password"])) {
                // password matches
                return $record;
            } else {
                // password does not match
                return false;
            }

			return $record; // Return column data

		} else {
			return false;
		}

        return false;	
	} // End of public function login(){

	public function checkEmailVerified($user_type,$username){
	
		$this->logger->debug('Class User checkEmailVerified()');
		
		$table = $this->getUserTypeDbTable($user_type);
		$this->logger->debug("user_type: [{$user_type}] table: [{$table}]");

		if($table != false){ // if not false

			// remove all whitespaces
			// See http://stackoverflow.com/questions/2109325/how-to-strip-all-spaces-out-of-a-string-in-php
			$username = preg_replace('/\s+/', '', $username);

	 		$record = $this->dbhelper->getSingleRowWhereSingleColumn(
				$table, // table name
				"username", // where column name
				"str", // where column datatype, int|str
				$username); // where column value

			if(!$record){
				$this->logger->error("getSingleRowWhereSingleColumn [{$table}] Failed.");
				return false;
			}

            if($record['signup_email_addr_verified'])
            	return true;
            else 
            	return false;

		} else {
			return false;
		}

        return false;
	}

    public function updatePasswordByRowId($password,$row_id,$user_table = "users"){

		$hashed_password = mysqli_real_escape_string($this->dbConn,$this->passwordEncrypt($password));

		// Update DB
		$now = date("Y-m-d H:i:s");
		$this->logger->debug("Updating password");
		$sql = "UPDATE {$user_table} SET updated_at='{$now}', updated_by='" . __FILE__ . "' ";
		$sql .= ", hashed_password='{$hashed_password}' ";
		// $sql .= "reset_password_token=NULL, reset_password_token_emailed_datetime=NULL ";
		$sql .= "WHERE id={$row_id} ";
		$this->logger->debug('$sql: [' . $sql . ']');

		$result = mysqli_query($this->dbConn, $sql);
		if(!$result) {
			
			$this->logger->error("SQL failed. Table: [users]. mysqli error: " . mysqli_error($this->dbConn));
			return false;

		} else {
			$this->logger->info("Updated Password");
		}

		return true;
	}

    public function updatePasswordByEmail($password,$email,$table){

		$hashed_password = mysqli_real_escape_string($this->dbConn,$this->passwordEncrypt($password));

		// Update DB
		$now = date("Y-m-d H:i:s");
		$this->logger->debug("Updating password");
		$sql = "UPDATE {$table} SET updated_at='{$now}', updated_by='" . __FILE__ . "' ";
		$sql .= ", hashed_password='{$hashed_password}' ";
		// $sql .= "reset_password_token=NULL, reset_password_token_emailed_datetime=NULL ";
		$sql .= "WHERE email_addr='{$email}' ";
		$this->logger->debug('$sql: [' . $sql . ']');

		$result = mysqli_query($this->dbConn, $sql);
		if(!$result) {
			
			$this->logger->error("SQL failed. Table: [users]. mysqli error: " . mysqli_error($this->dbConn));
			return false;

		} else {
			$this->logger->info("Updated Password");
		}

		return true;
	}

    public function resetPasswordEmail($email,$table,$user_type){

    	// Check if Email Exists
    	// $table = 'users';
		$safe_email = mysqli_real_escape_string($this->dbConn, $email);
		$user = $this->dbhelper->getSingleRowWhereSingleColumn(
			$table, // table name
			"email_addr", // where column name
			"str", // where column datatype, int|str
			$safe_email); // where column value

		if(!$user){
		 	$this->logger->error("Get Table [{$table}] Row Details Failed.");
			$this->logger->error("No such email in Users Table [{$email}]");
		 	return false;
		}

		// Email Exists
		$new_random_password = getRandomString(8);
		$this->logger->debug("New Random Password: [{$new_random_password}]");
		$ret = $this->updatePasswordByEmail($new_random_password,$email,$table);

		if(!$ret){
			return false;
		}

		// Send Email
    	$emailHelper = new EmailHelper($this->logger, $email);

    	$subject = "FHI LiveDifferent User Reset User Password";

		$content = "";
		$content .= "Dear User,\n";
		$content .= "\n";
		$content .= "Your password has been reset successfully reset.\n\n";
			
		$http_path = $this->http_path;
		if($user_type == "fhi_staff"){
			$http_path .= "/fhi";
		} else if ($user_type == "ae"){
			$http_path .= "/ae";
		} else if ($user_type == "poc_participant"){
			$http_path .= "";
		} else if ($user_type == "merchant"){
			$http_path .= "/merchant";
		}
		$content .= "Please click on following link to verify your email to login:\n";
		$content .= '<a href="' . $http_path . '/login.php">' . $http_path . '/login.php</a>'."\n\n";
		
		$content .= "Username: <b>" . $user['username'] . "</b>\n";
		$content .= "Password: <b>$new_random_password</b>\n";
		
    	$emailHelper->sendEmail($subject, $content);

    	return true;
    }

    public function sendSignUpVerificationEmail($row_id, $table, $type){

    	$this->logger->debug("sendSignUpVerificationEmail, row_id [{$row_id}] table [$table] type [$type]");

    	// Get User's Display Name and Email Address
    	$dbhelper = new DBHelper($this->logger,$this->dbConn);
	 	$row = $dbhelper->getSingleRowWhereSingleColumn(
			$table, // table name
			"id", // where column name
			"int", // where column datatype, int|str
			$row_id); // where column value
		
		if(!$row){
	 		$this->logger->error("getRowsCount [{$table}] Failed.");
	 		return false;
	  	}

	  	$display_name = $row['display_name']; // To get column data
	  	$email_addr = $row['email_addr']; // To get column data
	  	$username = $row['username']; // To get column data

	  	$this->logger->debug("display_name [{$display_name}] email_addr [{$email_addr}] username [{$username}]");

	  	// Get Server Settings HTTP_PATH
	  	$row = $dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"http_path"); // where column value
		
		if(!$row){
	 		$this->logger->error("getRowsCount [{$table}] Failed.");
	 		return false;
	  	}

	  	$http_path = $row['value']; // To get column data

	  	$this->logger->debug("http_path [{$http_path}]");

		$emailHelper = new EmailHelper($this->logger, $email_addr);

		// Prep Verification Email
		$random = generateRandomString().time();

		$subject = "FHI LiveDifferent User Sign Up Verification";
		if($type == "poc"){
			$type = "poc";
			$subject = "FHI LiveDifferent User Sign Up Verification";
			$sentence1 = "Welcome! Your FHI LiveDifferent portal user account has been created.";
		} else if ($type == "participant"){
			$type = "participant";
			$subject = "FHI LiveDifferent User Sign Up Verification";
			$sentence1 = "Your user account for FHI Client Portal has been created.";
			$sentence2 = "If you can complete the email verification before 7 pm today, LiveDifferent will be able to authenticate your account by 8 am the following day.\n\nYou can then proceed to use your account for registration of activities, eShop and many other functions.";
		} else if ($type == "users_ae"){
			$type = "ae";
			$subject = "FHI LiveDifferent AE User Sign Up Verification";
			$sentence1 = "Welcome! Your FHI LiveDifferent portal user account has been created.";
		} else if ($type == "fhi_staff"){
			$type = "fhi";
			$subject = "FHI LiveDifferent FHI User Sign Up Verification";
			$sentence1 = "Welcome! Your FHI LiveDifferent portal user account has been created.";
		} else if ($type == "poc_participant"){
			$type = "p";
			$subject = "FHI LiveDifferent User Sign Up Verification";
			$sentence1 = "Your user account for FHI Client Portal has been created.";
			$sentence2 = "If you can complete the email verification before 7 pm today, LiveDifferent will be able to authenticate your account by 8 am the following day.\n\nYou can then proceed to use your account for registration of activities, eShop and many other functions.";
		} else if ($type == "merchant"){
			$type = "me";
			$subject = "FHI LiveDifferent AE User Sign Up Verification";
			$sentence1 = "Welcome! Your FHI LiveDifferent portal user account has been created.";
		} else {
			$subject = "FHI LiveDifferent User Sign Up Verification";
			$sentence1 = "Welcome! Your FHI LiveDifferent portal user account has been created.";
		}

		$content = "";
		$content .= "Dear {$display_name},\n";
		$content .= "\n";
		$content .= $sentence1;
		$content .= "\n";
		$content .= "\n";
		$content .= "Please click on following link to verify your email to complete the user account creation.";
		$content .= "\n";
		
		$content .= '<a href="' . $http_path . '/verification/' . $type . '/' . $random . '">' . $http_path . '/verification/' . $type . '/' . $random . '</a>';
		$content .= "\n";
		// $content .= "Username: <b>" . $data['nric_fin'] . "</b>\n";
		
		$content .= "\n";
		$content .= "Username: <b>" . $username . "</b>";
		$content .= "\n";

		if($sentence2 != ""){
			$content .= "\n";
			$content .= $sentence2;
			$content .= "\n";
		}

		if(!$emailHelper->sendEmail($subject, $content)){
			$this->logger->error("Email Verification Sent Failed.");
		} else {
			$this->logger->info("Email Verification Sent Successful.");

			// Update DB
			$now = date("Y-m-d H:i:s");
			$this->logger->debug("Updating signup_email_addr_verify_token");
			$query = "UPDATE {$table} SET signup_email_addr_verify_token='{$random}', signup_email_addr_verify_emailed_datetime = '{$now}' WHERE id={$row_id} ";
			$this->logger->debug('$query: [' . $query . ']');

			$result = mysqli_query($this->dbConn, $query);
			if(!$result) {

				$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));
				return false;

			} else {
				$this->logger->debug("Updated");
			}

		}

		return true;
	}

	public function updateTermsAndConditionsAccepted($user_type, $user_id, $user_display_name, $terms_and_conditions_accepted){
		$this->logger->debug("updateTermsAndConditionsAccepted: user_type [{$user_type}] user_id: [{$user_id}] user_display_name: [{$user_display_name}] terms_and_conditions_accepted [{$terms_and_conditions_accepted}]");

		// $table = $this->getUserTypeDbTable($user_type);
		if($user_type == "fhi_staff"){
    		$table = "users";
    	} else if($user_type == "ae") {
    		$table = "users_ae";
    	} else if($user_type == "poc_participant") {
    		$table = "users_pp";
    	} else {
    		// $table = false;
    		$this->logger->error("Invalid user_type [{$user_type}].");
	 	 	// $return["ack"] = false;
	 	 	// $return["msg"] = "Update [{$table}] Failed.";
	 	 	// return $return;
	 	 	return false;
    	}

		$this->logger->debug("user_type: [{$user_type}] table: [{$table}]");

		// Update DB
		$date = date('Y-m-d H:i:s');

		if($terms_and_conditions_accepted){
			$set_fields_values[] = array( 
				"fieldname" => "t_c_accepted",
				"bColons" => false,
				"value" => 1);

			$set_fields_values[] = array( 
				"fieldname" => "t_c_accepted_datetime",
				"bColons" => true,
				"value" => $date);
		} else {
			$set_fields_values[] = array( 
				"fieldname" => "t_c_accepted",
				"bColons" => true,
				"value" => '0');

			$set_fields_values[] = array( 
				"fieldname" => "t_c_accepted_datetime",
				"bColons" => false,
				"value" => "NULL");
		}

		// Check if there are values to be update
		if(count($set_fields_values) < 1){

			$this->logger->warn('$set_fields_values count < 1. Not Updating.');
			// $json_res_data["ack"] = 1;
			// $json_res_data["err"] = 1;
			// $json_res_data["err_msg"] = "Missing Service ID.";

			// return $json_res_data;
			return false;
		}

		$set_fields_values[] = array( 
			"fieldname" => "updated_at",
			"bColons" => true,
			"value" => $date);

		$set_fields_values[] = array( 
			"fieldname" => "updated_by",
			"bColons" => true,
			"value" => $user_display_name);
		
		$where_fields_values = array(
     		array( 
      			"fieldname" => "id",
      	 		"bColons" => false,
      	  		"value" => $user_id)
		);

		$affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);
	  		
	  	if($affected_num_rows < 0){
	  		$this->logger->error("Update [{$table}] Failed.");
	 	 	// $return["ack"] = false;
	 	 	// $return["msg"] = "Update [{$table}] Failed.";
	 	 	// return $return;
	 	 	return false;
	  	} 

		return true;

	} // End of public function updateTermsAndConditionsAccepted($user_type, $user_id, $terms_and_conditions_accepted){

} // End of class Login

?>
