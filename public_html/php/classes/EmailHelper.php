<?php

class EmailHelper
{
	private $logger;

	private $email_addresses; // array of emails

	private $mail;
	private $host; // default email host
	private $smtpUsername; // default SMTP EMail Username
	private $smtpPassword; // default SMTP EMail Password
	private $smtpPort;	// Default SMTP Email Port

	public function __construct($logger, $email_addr){
		global $dbConn;
		$this->logger = $logger;
		$this->logger->info("Construct Email Object");

		$this->email_addresses = array($email_addr);

		$dbhelper = new DBHelper($logger,$dbConn);

		// Get Email Host
	  	$row = $dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"emaihelper_host"); // where column value
		
		if(!$row){
	 		$logger->error("getRowsCount from table [_server_settings] Failed.");
	 		$emaihelper_host = "feathertail.vodien.com";
	 		$logger->debug("setting to default emaihelper_host [{$emaihelper_host}]");
	  	} else {
	  		$emaihelper_host = $row['value']; // To get column data
	  		$logger->debug("emaihelper_host [{$emaihelper_host}]");
	  	}

	  	// Get Email Username
	  	$row = $dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"emaihelper_smtpUsername"); // where column value
		
		if(!$row){
	 		$logger->error("getRowsCount from table [_server_settings] Failed.");
	 		$emaihelper_smtpUsername = "livedifferent@fhi-online.com"; // To get column data
	  		$logger->debug("setting to default emaihelper_smtpUsername [{$emaihelper_smtpUsername}]");
	  	} else {
	  		$emaihelper_smtpUsername = $row['value']; // To get column data
	  		$logger->debug("emaihelper_smtpUsername [{$emaihelper_smtpUsername}]");
	  	}

	  	// Get Email Password
	  	$row = $dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"emaihelper_smtpPassword"); // where column value
		
		if(!$row){
	 		$logger->error("getRowsCount from table [_server_settings] Failed.");
	 		$emaihelper_smtpPassword = "live?different1"; // To get column data
	  		$logger->debug("setting to default emaihelper_smtpPassword [{$emaihelper_smtpPassword}]");
	  	} else {
	  		$emaihelper_smtpPassword = $row['value']; // To get column data
	  		$logger->debug("emaihelper_smtpPassword [{$emaihelper_smtpPassword}]");
	  	}

	  	// Get Email SMTP Port
	  	$row = $dbhelper->getSingleRowWhereSingleColumn(
			"_server_settings", // table name
			"name", // where column name
			"str", // where column datatype, int|str
			"emaihelper_smtpPort"); // where column value
		
		if(!$row){
	 		$logger->error("getRowsCount from table [_server_settings] Failed.");
	 		$emaihelper_smtpPort = 465; // To get column data
	  		$logger->debug("setting to default emaihelper_smtpPort [{$emaihelper_smtpPort}]");
	  	} else {
	  		$emaihelper_smtpPort = $row['value']; // To get column data
	  		$logger->debug("emaihelper_smtpPort [{$emaihelper_smtpPort}]");
	  	}

		// $this->host = "seabass.vodien.com";
		// $this->smtpUsername = "fhi-notify@edlution.com.sg";
		// $this->smtpPassword = "?ed1!lution?";
		// $this->smtpPort = 465;

		$this->host = $emaihelper_host;
		$this->smtpUsername = $emaihelper_smtpUsername;
		$this->smtpPassword = $emaihelper_smtpPassword;
		$this->smtpPort = $emaihelper_smtpPort;

		$this->mail = new PHPMailer_mime;

		$this->mail->isSMTP(); 				// Set mailer to use SMTP
	    $this->mail->Host = $this->host; 	// Specify main and backup SMTP servers
	    $this->mail->SMTPAuth = true;		// Enable SMTP authentication
	    $this->mail->Username = $this->smtpUsername;	// SMTP username
	    $this->mail->Password = $this->smtpPassword;	// SMTP password
	    $this->mail->SMTPSecure = 'ssl';	// Enable TLS encryption, `ssl` also accepted
	    $this->mail->Port = $this->smtpPort;	// TCP port to connect to

	    $this->mail->From = $this->smtpUsername;
    	// $this->mail->FromName = $this->smtpUsername;
    	$this->mail->FromName = 'Wine Delivery';
    	$this->mail->addReplyTo($this->smtpUsername, '');
        // $mail->addCC($this->smtpUsername);
    	// $mail->addBCC($this->smtpUsername); 	

    	$this->mail->WordWrap = 50; 	// Set word wrap to 50 characters
    	$this->mail->isHTML(true);                                  // Set email format to HTML
	} // End of public function __construct($logger,$dbConn,$mail){


	/**
	 * [sendEmail description]
	 * @param  [type] $subject [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 *
	 * inline example:
	 *
	 *  $emailHelper = new EmailHelper($this->logger, $this->email_addr);
	 *
	 *	$random = generateRandomString().time();
	 *	
	 *	$subject = "From Wine Delivery User Sign Up Verification";
	 *
	 *	$content = "";
	 *	$content .= "Dear {$this->display_name},\n";
	 *	$content .= "\n";
	 *	$content .= "Please click on following link to verify your email to complete the user account creation.\n";
	 *	$content .= "\n";
	 *	$content .= '<a href="https://' . gethostname() . '/verification/' . $random . '">https://' . gethostname() . '/verification/' . $random . '</a>';
 	 *  
	 *	if(!$emailHelper->sendEmail($subject, $content)){
	 *		$this->logger->error("Email Verification Sent Failed.");
	 *	} else {
	 *		$this->logger->info("Email Verification Sent Successful.");
 	 *
	 *		// Update DB
	 *		$now = date("Y-m-d H:i:s");
	 *		$this->logger->debug("Updating signup_email_addr_verify_token");
	 *		$query = "UPDATE users SET signup_email_addr_verify_token='{$random}', signup_email_addr_verify_emailed_datetime = '{$now}' WHERE user_id={$this->user_id} ";
	 *		$this->logger->debug('$query: ' . $query);
 	 *
	 *		$result = mysqli_query($this->dbConn, $query);
	 *		if(!$result) {
	 *					
	 *			$this->logger->error("SQL failed. Table: [{$table}]. mysqli error: " . mysqli_error($this->dbConn));
	 *			return false;
	 *
	 *		} else {
	 *			$this->logger->debug("Updated");
	 *		}
 	 *
	 *	}
	 *
	 *	return true;
	 * 
	 */
	public function sendEmail($subject, $content, $attachment = ''){
		$this->logger->info("Send Email");

		$this->mail->ClearAllRecipients(); // clear added emails

		foreach($this->email_addresses as $value){
			$this->mail->addAddress($value);     // Add a recipient
		}

		// Set email default writing
	    $now = date("d-M-Y H:i");

	    $this->mail->Subject = $subject; // Set Subject

	    $content .= "\n";

		$content .= "This automatic message was sent by Wine Delivery on {$now} \n";
		
		$content .= "\n";
		
		$content .= "Sincerely,\n";
		$content .= "Wine Delivery\n";

		$body_content = nl2br($content);
		$body_content = '<span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px">'.$body_content.'</span>';

		$this->mail->Body    = $body_content; // Set Body
    	$this->mail->AltBody = $body_content;

    	if($attachment != ''){
    		$this->logger->info("Add Attachment: [{$attachment}]");
    		$this->mail->AddAttachment($attachment);
    	}

    	if(!$this->mail->send()) {
		
	        $this->logger->error("Message could not be sent.");
			$this->logger->error('Mailer Error: ' . $this->mail->ErrorInfo);

			return false;

	    }
	    
	    $this->logger->info("Email has been sent");

	    //Connect to IMAP
		$imap_sent_folder = "{".$this->host."}INBOX.Sent";
		$stream = imap_open($imap_sent_folder, $this->smtpUsername, $this->smtpPassword);
		$mail_mime = $this->mail->get_mail_string();
		imap_append($stream, $imap_sent_folder, $mail_mime);
		imap_close($stream);

		return true;

	} // End of public function sendEmail($email, $content){
}

?>
