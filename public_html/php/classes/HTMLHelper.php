<?php 

class HTMLHelper{

	private $logger;
	private $dbConn;

	private $dbhelper;

	public function __construct($logger, $dbConn){

		$this->logger = $logger;
		$this->dbConn = $dbConn;

		$this->dbhelper = new DBHelper($this->logger,$this->dbConn);

	}

	public function generateCountryDialingCodelist($selected = null){
	// public function generateCountryDialingCodelist(){
		if(isset($selected))
			$this->logger->debug("Selected: [{$selected}]");
		else
			$this->logger->debug("Selected: [null]");

		$html = "";

		$returnedArray = $this->dbhelper->getAllRows("country_dialing_codes");

		foreach($returnedArray as $x => $x_value) {
		    // $this->logger->debug(" Key=[" . $x . "], dialing_code_id=[" . $x_value['dialing_code_id'] . "], dialing_code=[" . $x_value['dialing_code'] . "], country=[" . $x_value['country'] . "], sequence_id=[" . $x_value['sequence_id'] . "]" );
		
		    if($x_value['dialing_code'] == $selected){
		    	// $this->logger->debug("Selected");
		    	$html .= '<option value="' . $x_value['dialing_code'] . '" selected="selected">'. $x_value['dialing_code'] . " " . $x_value['country'] .'</option>' . "\n";
		    }else
		    	$html .= '<option value="' . $x_value['dialing_code'] . '">'. $x_value['dialing_code'] . " " . $x_value['country'] .'</option>' . "\n";
		}

		return $html;

	}

	public function generateCountrylist($selected = null){

		if(isset($selected))
			$this->logger->debug("Selected: [{$selected}]");
		else
			$this->logger->debug("Selected: [null]");

		$html = "";

		$returnedArray = $this->dbhelper->getAllRows("country_dialing_codes");

		foreach($returnedArray as $x => $x_value) {
		    // $this->logger->debug(" Key=[" . $x . "], dialing_code_id=[" . $x_value['dialing_code_id'] . "], dialing_code=[" . $x_value['dialing_code'] . "], country=[" . $x_value['country'] . "], sequence_id=[" . $x_value['sequence_id'] . "]" );
		
		    if($x_value['dialing_code'] == $selected){
		    	// $this->logger->debug("Selected");
		    	$html .= '<option value="' . $x_value['country'] . '" selected>'. $x_value['country'] .'</option>' . "\n";
		    }else
		    	$html .= '<option value="' . $x_value['country'] . '">' . $x_value['country'] .'</option>' . "\n";
		}

		return $html;

	}

}

?>
