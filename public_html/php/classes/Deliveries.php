<?php 

class Deliveries
{

    private $logger;
    private $dbConn;

    private $dbhelper;

    public function __construct($logger, $dbConn){

        $this->logger = $logger;
        $this->dbConn = $dbConn;

        $this->dbhelper = new DBHelper($this->logger,$this->dbConn);

    }

    public function create($data){

    } // End of public function create($data)

    public function update($data){


        $this->logger->debug("Start: Deliveries Class update fn:");
        $this->logger->debug($data);

        $this->logger->debug("user_id: [{$data['user_id']}] user_display_name: [{$data['user_display_name']}]");

        // Get Key Column Value
        if(isset($data['status']) && exists($data['status'])){

        }
        else {
            $this->logger->error("Missing status.");
            $json_res_data["ack"] = 1;
            $json_res_data["err"] = 1;
            $json_res_data["err_msg"] = "Missing status.";

            return $json_res_data;
        }

        $data['signature'] = explode(',', $data['signature']);
        $decoded_signature = base64_decode($data['signature'][1]);
        $image = "/wd/consumer_signatures/" . $data['sale_order_num'] . ".jpg";
        $this->logger->debug($image);
        $fp = fopen( $image, 'w' );
        fwrite( $fp, $decoded_signature);
        fclose( $fp );  


        $table = "orders";

        $where_array = array();

        $where_array[] = array( 
            "fieldname" => "sale_order_num",
            "bColons" => true,
            "value" => $data['sale_order_num']);

        $rows_count = $this->dbhelper->getRowsCount($table,$where_array);

        if($rows_count < 0){
            $this->logger->error("getRowsCount table [{$table}] Failed.");
            $json_res_data["ack"] = 1;
            $json_res_data["err"] = 1;
            $json_res_data["err_msg"] = "getRowsCount table [{$table}] Failed.";
            return $json_res_data;
        }

        if($rows_count > 0){

        }


        // Reach here means there were no errors

        // For Update for Table [users_merchants]
        $set_fields_values = array();

        if(isset($data['status']) && exists($data['status'])){
            $set_fields_values[] = array( 
                "fieldname" => "status",
                "bColons" => true,
                "value" => $data['status']);
        }

        if(count($set_fields_values) > 0){

            $table = "orders";

            $set_fields_values[] = array(
                "fieldname" => "updated_at",
                "bColons" => false,
                "value" => 'NOW()');

            $set_fields_values[] = array(
                "fieldname" => "updated_by",
                "bColons" => true,
                "value" => $data['user_display_name']);
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed",
                "bColons" => false,
                "value" => 1);
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed_datetime",
                "bColons" => false,
                "value" => 'NOW()');
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed_customer_id",
                "bColons" => true,
                "value" => $data['consumer_id']);
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed_customer_name",
                "bColons" => true,
                "value" => $data['first_name'] . " " . $data['last_name']);
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed_received_by_user_id",
                "bColons" => false,
                "value" => $data['user_id']);
            
            $set_fields_values[] = array(
                "fieldname" => "has_signed_received_by_user_display_name",
                "bColons" => true,
                "value" => $data['user_display_name']);
            

            // SQL Where
            $where_fields_values = array(
                array( 
                    "fieldname" => "sale_order_num",
                    "bColons" => true,
                    "value" => $data['sale_order_num'])
            );

            $affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);

            if($affected_num_rows < 0){

                $this->logger->warn('Update [{$table}] Failed.');
                $json_res_data["ack"] = 1;
                $json_res_data["err"] = 1;
                $json_res_data["err_msg"] = 'Update [{$table}] Failed.';

                return $json_res_data;
            }

        } else {

            $this->logger->info("No fields values to update. Table [{$table}] User ID [{$user_id}]");

        }


        // END: For Update for Table [merchants_orders]

    } // End of public function update($data){

    public function updateList($data){


        $this->logger->debug("Start: Deliveries Class updateList fn:");
        
        foreach($data['data'] as $key => $value){
            $this->logger->debug("sale_order_num: [{$value['sale_order_num']}] status: [{$value['status']}] user_id: [{$value['user_id']}] user_display_name: [{$value['user_display_name']}]");    
            
            // Get Key Column Value
            if(isset($value['status']) && exists($value['status'])){
            
            }
            else {
                $this->logger->error("Missing status.");
                $this->logger->error("Continuing to next array.");

                continue;
            }
            
            $this->logger->debug("Decoding signature and saving to [/wd/consumer_signatures/{$value['sale_order_num']}.jpg]");
            $value['signature'] = explode(',', $value['signature']);
            $decoded_signature = base64_decode($value['signature'][1]);
            $image = "/wd/consumer_signatures/" . $value['sale_order_num'] . ".jpg";
            $fp = fopen( $image, 'w' );
            fwrite( $fp, $decoded_signature);
            fclose( $fp );
            
            $table = "orders";
            
            $where_array = array();

            $where_array[] = array( 
                "fieldname" => "sale_order_num",
                "bColons" => true,
                "value" => $value['sale_order_num']);

            $rows_count = $this->dbhelper->getRowsCount($table,$where_array);

            if($rows_count < 0){
                $this->logger->error("getRowsCount table [{$table}] Failed.");
                $this->logger->error("Continuing to next array.");
                
                continue;
            }

            // Reach here means there were no errors
            
            
            // For Update for Table [orders]
            $set_fields_values = array();

            if(isset($value['status']) && exists($value['status'])){
                $set_fields_values[] = array( 
                    "fieldname" => "status",
                    "bColons" => true,
                    "value" => $value['status']);
            }
            
            if(count($set_fields_values) > 0){

                $table = "orders";

                $set_fields_values[] = array(
                    "fieldname" => "updated_at",
                    "bColons" => false,
                    "value" => 'NOW()');

                $set_fields_values[] = array(
                    "fieldname" => "updated_by",
                    "bColons" => true,
                    "value" => $value['user_display_name']);

                $set_fields_values[] = array(
                    "fieldname" => "has_signed",
                    "bColons" => false,
                    "value" => 1);

                $set_fields_values[] = array(
                    "fieldname" => "has_signed_datetime",
                    "bColons" => false,
                    "value" => 'NOW()');

                $set_fields_values[] = array(
                    "fieldname" => "has_signed_customer_id",
                    "bColons" => true,
                    "value" => $value['consumer_id']);

                $set_fields_values[] = array(
                    "fieldname" => "has_signed_customer_name",
                    "bColons" => true,
                    "value" => $value['first_name'] . " " . $value['last_name']);

                $set_fields_values[] = array(
                    "fieldname" => "has_signed_received_by_user_id",
                    "bColons" => false,
                    "value" => $value['user_id']);

                $set_fields_values[] = array(
                    "fieldname" => "has_signed_received_by_user_display_name",
                    "bColons" => true,
                    "value" => $value['user_display_name']);


                // SQL Where
                $where_fields_values = array(
                    array( 
                        "fieldname" => "sale_order_num",
                        "bColons" => true,
                        "value" => $value['sale_order_num'])
                );

                $affected_num_rows = $this->dbhelper->updateRow($table,$set_fields_values,$where_fields_values);

                if($affected_num_rows < 0){
                    $this->logger->warn('Update [{$table}] Failed.');
                }
            }
            else {

                $this->logger->info("No fields values to update. Table [{$table}] User ID [{$user_id}]");

            }
        }
        // END: foreach($data['data'] as $key => $value){

    } // End of public function updateList($data){

    public function delete($data){

    } // End of public function delete($data){

}

?>