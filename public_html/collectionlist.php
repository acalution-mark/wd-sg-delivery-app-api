<html>
    <head>
        <title>Collection List!</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style>
            #collection-list {
                width: calc(50% - 4px);
                float:left;
                border: 1px black solid;
            }
            #collection-order {
                width: calc(50% - 4px);
                float:right;
                border: 1px black solid;
            }
        </style>
    </head>
    <body>
        <div>Simple Collection Page</div>
        <button type="button" id="collection-list-button">Load Collection List!</button>
        <hr>
        <div id="collection-wrapper" >
            <div id="collection-list" >
            </div>
            <div id="collection-order" >
            </div>
        </div>
        <script>
            $( document ).ready(function() {

                $( "#collection-list-button" ).click(function() {

                    var JSONObj = {
                        "start_date_time":"10-11-2016",
                        "end_date_time":"15-11-2016",
                    };

                    var st = JSON.stringify(JSONObj);
                    
                    $.ajax({
                        type: "POST",
                        url: 'http://' + 'wd-dev2-deliapp.edlution.com.sg' + '/php/api/collection_r_l.php',
                        data: { data : st },
                        dataType   : 'json',
                        success: function(response) {
                            console.log(response);

                            if(response.ack == 1) {

                                $( "#collection-list" ).empty();
                                $.each(response.data, function(key, value) {
                                    //console.log(key, value);
                                    $( "#collection-list" ).append( '<p>' );

                                    $( "#collection-list" ).append( '<span> ' );
                                    $( "#collection-list" ).append( value.collection_datetime );
                                    $( "#collection-list" ).append( ' </span>' );

                                    $( "#collection-list" ).append( '<span> ' );
                                    $( "#collection-list" ).append( value.purchase_order_num );
                                    $( "#collection-list" ).append( ' </span>' );

                                    $( "#collection-list" ).append( '<span> ' );
                                    $( "#collection-list" ).append( value.status );
                                    $( "#collection-list" ).append( ' </span>' );

                                    $( "#collection-list" ).append( '<span> ' );
                                    $( "#collection-list" ).append( "<button type='button' class='collection-button' data-ponumber='" + value.purchase_order_num + "' >Load Collection Order</button>" );
                                    $( "#collection-list" ).append( ' </span>' );

                                    $( "#collection-list" ).append( '</p>' );
                                });
                            }

                            $( ".collection-button" ).click(function() {

                                var ponumber = $(this).attr("data-ponumber");
                                var JSONObj = {
                                    "purchase_order_num":ponumber
                                };

                                var st = JSON.stringify(JSONObj);

                                console.log(st);
                                $.ajax({
                                    type: "POST",
                                    url: 'http://' + 'wd-dev2-deliapp.edlution.com.sg' + '/php/api/collection_r_s.php',
                                    // crossDomain: true,
                                    // data: { data : data },
                                    data: { data : st },
                                    dataType   : 'json',
                                    success: function(response) {
                                        console.log(response);
                                        if(response.ack == 1) {
                                            $( "#collection-order" ).empty();
                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.address_line_1 );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.address_line_2 );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.postal_code );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.first_name );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.last_name );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.contact_num );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.shop_status );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<p>' );
                                            $( "#collection-order" ).append( response.data.collection_status );
                                            $( "#collection-order" ).append( '</p>' );

                                            $( "#collection-order" ).append( '<span> ' );
                                            $( "#collection-order" ).append( "<button type='button' class='collection-edit' data-ponumber='" + ponumber + "' >COLLECTED</button>" );
                                            $( "#collection-order" ).append( ' </span>' );
                                        }

                                        $( ".collection-edit" ).click(function() {
                                            var ponumber = $(this).attr("data-ponumber");
                                            var JSONObj = {
                                                "purchase_order_num":ponumber,
                                                "status": "COLLECTED",
                                                "user_display_name": "Larry Simple API",
                                                "user_id": "2",
                                                "action": "update"
                                            };

                                            var st = JSON.stringify(JSONObj);

                                            console.log(st);

                                            $.ajax({
                                                type: "POST",
                                                url: 'http://' + 'wd-dev2-deliapp.edlution.com.sg' + '/php/api/collection_cud.php',
                                                // crossDomain: true,
                                                // data: { data : data },
                                                data: { data : st },
                                                dataType   : 'json',
                                                success: function(response) {
                                                    console.log(response);
                                                    if(response.ack == 1) {
                                                        
                                                    }
                                                },
                                                error: function() {
                                                    console.log("Process Inputs Error.");
                                                }
                                            });
                                        });
                                    },
                                    error: function() {
                                        console.log("Process Inputs Error.");
                                    }
                                });

                            });
                        },
                        error: function() {
                            console.log("Process Inputs Error.");
                        }
                    });
                });

            });
        </script>
    </body>
</html>

